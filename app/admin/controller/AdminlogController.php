<?php

/**
 * Administrator Log
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class AdminlogController extends AdminbaseController {

    //List
    function index(){
        
        $data = $this->request->param();
        $map=[];
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $adminid= $data['adminid'] ?? '';
        if($adminid!=''){
            $map[]=['adminid','=',$adminid];
        }
			

    	$lists = Db::name("admin_log")
                ->where($map)
                ->order("id DESC")
                ->paginate(20);
                
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }

    //Delete
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('admin_log')
            ->where("id={$id}")
            ->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        $this->success(lang("DELETE_SUCCESS"));
        							  			
    }

    //Export
    function export(){

        $data = $this->request->param();
        $map=[];
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $adminid= $data['adminid'] ?? '';
        if($adminid!=''){
            $map[]=['adminid','=',$adminid];
        }

        $xlsName  = "Administrator Log";
        $xlsData=Db::name("admin_log")
            ->where($map)
            ->order("addtime DESC")
            ->select()
            ->toArray();

        if(empty($xlsData)){
            $this->error(lang("DATA_EMPTY"));
        }
        
        foreach ($xlsData as $k => $v) {

            $xlsData[$k]['ip']=long2ip($v['ip']);
            $xlsData[$k]['addtime']=date("Y-m-d H:i:s",$v['addtime']);
        }
                $cellName = array('A','B','C','D','E');
                $xlsCell  = array(
                    array('id', lang('SERIAL_NUMBER')),
                    array('admin', lang('ADMINISTRATOR')),
                    array('action', lang('ACTION')),
                    array('ip', 'IP'),
                    array('addtime', lang('SUBMISSION_TIME')),
        );
        exportExcel($xlsName,$xlsCell,$xlsData,$cellName);
    }
    
}

<?php

/**
 * Advertising space
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use think\db\Query;
use cmf\lib\Upload;


class AdvertController extends AdminbaseController {

	/*Ad list*/
    public function index(){

		$p = $this->request->param('p');
		if(!$p){
			$p=1;
		}
		$lists = Db::name('video')
            ->where(function (Query $query) {
				$data = $this->request->param();
				$query->where('is_ad', '1');
				$query->where('isdel', '0');

				$keyword= $data['keyword'] ?? '';

                if (!empty($keyword)) {
                    $query->where('uid|id', '=' , $keyword);
                }
             	
             	$keyword1= $data['keyword1'] ?? '';

                if (!empty($keyword1)) {
                    $query->where('title', 'like', "%$keyword1%");
                }

                $keyword2= $data['keyword2'] ?? '';
				
				if (!empty($keyword2)) {
					$userlist =Db::name("user")->field("id")
						->where("user_nickname like '%".$keyword2."%'")
						->select();
					$strids="";
					foreach($userlist as $ku=>$vu){
						if($strids==""){
							$strids=$vu['id'];
						}else{
							$strids.=",".$vu['id'];
						}
					}					
                    $query->where('uid', 'in', $strids);
                }

            })
            ->order("orderno desc,addtime DESC")
            ->paginate(20);
			
		$lists->each(function($v,$k){
			if($v['uid']==0){
				$userinfo=array(
					'user_nickname'=>lang('SYSTEM_ADMINISTRATOR')
				);
			}else{
				$userinfo=getUserInfo($v['uid']);
				if(!$userinfo){
					$userinfo=array(
						'user_nickname'=>lang('DELETE')
					);
				}
				
			}
			$v['userinfo']=$userinfo;
            $v['thumb']=get_upload_path($v['thumb']);
			$ad_endtime='';
			if($v['ad_endtime'] == 0){
				$v['ad_endtime']='---';
			}else{
				$ad_endtime=(int)$v['ad_endtime'];
				$v['ad_endtime']=date('Y-m-d',$ad_endtime);
			}
	
			return $v;
			
		});
		
		//Paging--> Filter parame
		$data = $this->request->param();
		$lists->appends($data);	

        // Get paging display
        $page = $lists->render();
		
    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	$this->assign("p",$p);
    	return $this->fetch();
    }

	//Delete video
	public function del(){

		$res=array("code"=>0,"msg"=>lang('DELETE_SUCCESS'),"info"=>array());
		
		$data = $this->request->param();
		
		$id=$data['id'];
		$reason=$data["reason"];
		
		if(!$id){
			$res['code']=1001;
			$res['msg']=lang('VIDEO_INFO_FAILED_TO_LOAD');
			echo json_encode($res);
			return;
		}
		$result=Db::name("video")->where("id={$id}")->delete();

		if($result!==false){

			Db::name("video_comments")->where("videoid={$id}")->delete();	 	//Delete video comment
			Db::name("video_like")->where("videoid={$id}")->delete();	 		//Delete video likes
			Db::name("video_report")->where("videoid={$id}")->delete();	 		//Delete video report
			Db::name("video_comments_like")->where("videoid={$id}")->delete(); 	//Delete video comment like

			$res['msg']=lang('THE_AD_DELETED_SUCCESS');
			echo json_encode($res);
			
		}else{
			$res['code']=1002;
			$res['msg']=lang('THE_AD_DELETED_FAILED');
			echo json_encode($res);
		}			
										  			
	}		
   
	//Add ads
	public function add(){
		//Get advertising users
		$adLists=Db::name("user")
			->field("id,user_nickname")
			->where("user_status=1 and user_type=2 and is_ad=1")
			->select();

		$this->assign("adLists",$adLists);
		
		return $this->fetch();				
	}

	public function addPost(){
		if($this->request->isPost()) {
			$data = $this->request->param();	

			$data['addtime']=time();
			$data['ad_endtime']=strtotime($data["ad_endtime"]);
			$data['is_ad']=1;
			$data['status']=1;
			$data['ad_url']=html_entity_decode($data["ad_url"]);//Convert html entities to characters

			$owner_uid='';
			
			if(!isset($data['owner_uid'])){
				$this->error(lang('PLEASE_ADD_VIDEO_PUBLISHER_FIRST'));
			}

			$owner_uid=$data['owner_uid'];

			if($owner_uid==""||!is_numeric($owner_uid)){
				$this->error(lang('PLEASE_FILL_VIDEO_OWNER_ID'));
			}

			//Determine whether the user exists
			$ownerInfo=Db::name("user")
				->where("user_type=2 and id={$owner_uid} and is_ad=1")
				->find();
			if(!$ownerInfo){
				$this->error(lang('AD_PUBLISHER_NOT_EXIST'));
			}
			
			$data['uid']=$owner_uid;

			$url=trim($data['href']);
			$title=$data['title'];
			$thumb=$data['thumb'];
			$ad_url=$data['ad_url'];

			if($title==""){
				$this->error(lang('PLEASE_FILL_AD_TITLE'));
			}
			if($thumb==""){
				$this->error(lang('PLEASE_UPLOAD_AD_COVER'));
			}

			$data['thumb']=set_upload_path($thumb);
			$data['thumb_s']=set_upload_path($thumb);

			$video_upload_type=$data['video_upload_type'];
			$uploadSetting = cmf_get_upload_setting();
            $extensions=$uploadSetting['file_types']['video']['extensions'];
            $allow=explode(",",$extensions);

            if($video_upload_type==0){ //Video link

            	if(!$url){
					$this->error(lang('PLEASE_FILL_VIDEO_URL'));
            	}
            	//Judge the correctness of the link address
                if(strpos($url,'http')===false){
					$this->error(lang('PLEASE_PROVIDE_CORRECT_VIDEO_URL'));
                }
                $video_type=substr(strrchr($url, '.'), 1);

                if(!in_array(strtolower($video_type), $allow)){
					$this->error(lang('PLEASE_PROVIDE_VALID_VIDEO_URL'));
                }

				$data['href']=$url;
				$data['href_w']=$url;

            }else{ //Video file

            	//Get background upload configuration
				$configpri=getConfigPri();
				if(!isset($_FILES)){
					$this->error(lang('PLEASE_UPLOAD_VIDEO'));
				}

				if(!isset($_FILES["file"])){
					$this->error(lang('PLEASE_UPLOAD_VIDEO'));
				}

				if(!$_FILES["file"]){
                    $this->error(lang('PLEASE_UPLOAD_VIDEO'));
                }
                
                $files["file"]=$_FILES["file"];
                $file=$files["file"];

                if (!get_file_suffix($file['name'],$allow)){
                    $this->error(lang('PLEASE_UPLOAD_CORRECT_VIDEO_FORMAT_OR_CHECK_UPLOAD_SETTINGS'));
                }

                $configpri=getConfigPri();
        		$cloudtype=$configpri['cloudtype'];
        		if($cloudtype==1){
        			$uploader = new Upload();
		            $uploader->setFileType('video');
		            $res = $uploader->upload();
		            if($res===false){
		               $this->error(lang('FILE_UPLOAD_FAILED'));
		            }

        		}else{
        			$res=adminUploadFiles($file,2);
		            if($res===false){
		               $this->error(lang('FILE_UPLOAD_FAILED'));
		            }
        		}

				
				$data['href']=set_upload_path($res['filepath']);
				$data['href_w']=set_upload_path($res['filepath']);
            }
			
			if(!$ad_url){
				$this->error(lang('PLEASE_FILL_IN_THE_AD_LINK'));
			}
			
			unset($data['file']);
			unset($data['video_upload_type']);
			unset($data['owner_uid']);

			$result=Db::name("video")->insert($data);

			if($result){
				$this->success(lang('ADD_SUCCESS'), url('Advert/index'),3);
			}else{
				$this->error(lang('FAILED_TO_ADD'));
			}
		}			
	}
	
	
	//Edit video
	public function edit(){
		
		$data = $this->request->param();
		$id=intval($data['id']);
		$from=$data["from"];
		if($id){
			$video=Db::name("video")->where("id={$id}")->find();
			
			$userinfo=getUserInfo($video['uid']);
			if(!$userinfo){
				$userinfo=array(
					'user_nickname'=>lang('DELETE')
				);
			}
			
			$video['userinfo']=$userinfo;
            $video['thumb']=get_upload_path($video['thumb']);
            $video['href']=get_upload_path($video['href']);
            $video['href_w']=get_upload_path($video['href_w']);
            if($video['ad_endtime'] == 0){
				$video['ad_endtime']='';
			}else{
				$ad_endtime=(int)$video['ad_endtime'];
				$video['ad_endtime']=date('Y-m-d',$ad_endtime);
				
			}
			$this->assign('video', $video);						
		}else{				
			$this->error(lang('DATA_TRANSFER_FAILED'));
		}
		$this->assign("from",$from);
		return $this->fetch();				
	}

	
	public function editPost(){
		if($this->request->isPost()) {
	
			$data = $this->request->param();

			$id=$data['id'];
			$title=$data['title'];
			$thumb=$data['thumb'];
			$thumb_old=$data['thumb_old'];
			$url=$data['href_e'];
			$type='';

			if(isset($data['video_upload_type'])){
				$type=$data['video_upload_type'];
			}

			$data['ad_endtime']=strtotime($data["ad_endtime"]);
			$data['ad_url']=html_entity_decode($data["ad_url"]);//Convert html entities to characters

			if($thumb==""){
				$this->error(lang('PLEASE_UPLOAD_VIDEO_COVER'));
			}

			if($thumb!=$thumb_old){
				$data['thumb']=set_upload_path($thumb);
				$data['thumb_s']=set_upload_path($thumb);
			}
			
			$ad_url=$data['ad_url'];

			if($type!=''){

				$uploadSetting = cmf_get_upload_setting();
	            $extensions=$uploadSetting['file_types']['video']['extensions'];
	            $allow=explode(",",$extensions);

				if($type==0){ //Video link type
					if($url==''){
						$this->error(lang('PLEASE_FILL_IN_VIDEO_LINK'));
					}

					//Judge the correctness of the link address
					if(strpos($url,'http')!==false||strpos($url,'https')!==false){

						$video_type=substr(strrchr($url, '.'), 1);

						if(!in_array(strtolower($video_type), $allow)){
							$this->error(lang('PLEASE_FILL_IN_CORRECT_SUFFIX_VIDEO_URL'));
		                }

						$data['href']=$url;
						$data['href_w']=$url;
					}else{
						$this->error(lang('PLEASE_FILL_IN_CORRECT_VIDEO_URL'));
					}


				}else if($type==1){ //File upload type

					if(!$_FILES["file"]){
                        $this->error(lang('PLEASE_UPLOAD_VIDEO'));
                    }
                    
                    $files["file"]=$_FILES["file"];

                    if (!get_file_suffix($files['file']['name'],$allow)){
	                    $this->error(lang('PLEASE_UPLOAD_CORRECT_VIDEO_FORMAT_OR_CHECK_UPLOAD_SETTINGS'));
	                }

	                $configpri=getConfigPri();
        			$cloudtype=$configpri['cloudtype'];
                    
                    if($cloudtype==1){
	        			$uploader = new Upload();
			            $uploader->setFileType('video');
			            $res = $uploader->upload();
			            if($res===false){
			               $this->error(lang('FILE_UPLOAD_FAILED'));
			            }

	        		}else{
	        			$res=adminUploadFiles($file,2);
			            if($res===false){
			               $this->error(lang('FILE_UPLOAD_FAILED'));
			            }

	        		}

	        		$data['href']=set_upload_path($res['filepath']);
					$data['href_w']=set_upload_path($res['filepath']);
                    
				}
			}

			if(!$ad_url){
				$this->error(lang('PLEASE_FILL_IN_THE_AD_LINK'));
			}
			
			unset($data['file']);
			unset($data['href_e']);
			unset($data['video_upload_type']);
			unset($data['owner_uid']);
			unset($data['thumb_old']);
			unset($data['ckplayer_playerzmblbkjP']);

			$result=Db::name("video")->update($data);
			if($result!==false){
				$this->success(lang('UPDATE_SUCCESS'));
			 }else{
				$this->error(lang('FAILED_TO_EDIT'));
			 }
		}			
	}

    //Set off the shelf
    public function setXiajia(){
		$res = array("code" => 0, "msg" => lang('UNSHELVED_SUCCESS'), "info" => array());
		$data = $this->request->param();

    	$id=$data['id'];
    	$reason=$data["reason"];
    	if(!$id){
    		$res['code']=1001;
    		$res['msg']=lang('PLEASE_CONFIRM_THE_VIDEO_INFORMATION');
    		echo json_encode($res);
            return;
    	}

    	//Determine whether this video exists
    	$videoInfo=Db::name("video")->where("id={$id}")->find();
    	if(!$videoInfo){
    		$res['code']=1001;
    		$res['msg']=lang('PLEASE_CONFIRM_THE_VIDEO_INFORMATION');
    		echo json_encode($res);
            return;
    	}

    	//Update video status
    	$data=array("isdel"=>1,"xiajia_reason"=>$reason);

    	$result=Db::name("video")->where("id={$id}")->update($data);

    	if($result!==false){

    		//Change the status of the video like list
    		Db::name("video_like")->where("videoid={$id}")->update(['status'=>0]);

    		//Update the report information for this video
    		$data1=array(
    			'status'=>1,
    			'uptime'=>time()
    		);

    		Db::name("video_report")->where("videoid={$id}")->update($data1);

    		echo json_encode($res);
    		
    	}else{
    		$res['code']=1002;
			$res['msg'] = lang('UNSHELVING_FAILED');
    		echo json_encode($res);
    		
    	}
    	
    }

    /*List of removed videos*/
    public  function lowervideo(){

		$p = $this->request->param('p');
		if(!$p){
			$p=1;
		}
		$lists = Db::name('video')
            ->where(function (Query $query) {
				$data = $this->request->param();
				$query->where('is_ad', '1');
				$query->where('isdel', '1');

				$keyword= $data['keyword'] ?? '';

                if (!empty($keyword)) {
                    $query->where('uid|id', '=' , $keyword);
                }

                $keyword1= $data['keyword1'] ?? '';
             
                if (!empty($keyword1)) {
                    $query->where('title', 'like', "%$keyword1%");
                }

                $keyword2= $data['keyword2'] ?? '';
				
				if (!empty($keyword2)) {
					$userlist =Db::name("user")->field("id")
						->where("user_nickname like '%".$keyword2."%'")
						->select();
					$strids="";
					foreach($userlist as $ku=>$vu){
						if($strids==""){
							$strids=$vu['id'];
						}else{
							$strids.=",".$vu['id'];
						}
					}					
                    $query->where('uid', 'in', $strids);
                }

            })
            ->order("orderno desc,addtime DESC")
            ->paginate(20);
			
		$lists->each(function($v,$k){
			if($v['uid']==0){
				$userinfo=array(
					'user_nickname'=>lang('SYSTEM_ADMINISTRATOR')
				);
			}else{
				$userinfo=getUserInfo($v['uid']);
				if(!$userinfo){
					$userinfo=array(
						'user_nickname'=>lang('DELETE')
					);
				}
				
			}
			$v['userinfo']=$userinfo;
            $v['thumb']=get_upload_path($v['thumb']);

			$ad_endtime='';
			if($v['ad_endtime'] == 0){
				$v['ad_endtime']='---';
			}else{
				$ad_endtime=(int)$v['ad_endtime'];
				$v['ad_endtime']=date('Y-m-d',$ad_endtime);
				
			}
	
			return $v;
			
		});
			
		//Paging--> Filter parameter
		$data = $this->request->param();
		$lists->appends($data);		
        // Get paging display
        $page = $lists->render();
		
    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	$this->assign("p",$p);
    	return $this->fetch();
    }

	//Watch the video
    public function  video_listen(){
		
		$id = $this->request->param('id');
    	if(!$id||$id==""||!is_numeric($id)){
			$this->error(lang('LOADING_FAILED'));
    	}else{
    		//Get music information
    		$info=Db::name("video")->where("id={$id}")->find();
            $info['thumb']=get_upload_path($info['thumb']);
            $info['href']=get_upload_path($info['href']);
    		$this->assign("info",$info);
    	}

    	return $this->fetch();
    }

    /*Video on shelves*/
    public function set_shangjia(){
    	$id = $this->request->param('id');
    	if(!$id){
    		$this->error(lang('VIDEO_INFO_LOADING_FAILED'));
    	}

    	//Get video information
    	$info=Db::name("video")->where("id={$id}")->find();
    	if(!$info){
    		$this->error(lang('VIDEO_INFO_LOADING_FAILED'));
    	}

    	$data=array(
    		'xiajia_reason'=>'',
    		'isdel'=>0
    	);
    	$result=Db::name("video")->where("id={$id}")->update($data);
    	if($result!==false){
    		//Change the status of the video like list
    		Db::name("video_like")->where("videoid={$id}")->update(['status'=>1]);

			$this->success(lang('ITEM_UP_SUCCESS'));
    	}
    	return $this->fetch();
    }

    //List of comments
	public function commentlists(){
		
		$data = $this->request->param();
    	$videoid=$data['videoid'];
		
		$lists = Db::name('video_comments')
            ->where("videoid={$videoid}")
            ->order("addtime DESC")
            ->paginate(20);
			
	
		$lists->each(function($v,$k){
		
			$userinfo=getUserInfo($v['uid']);
			if(!$userinfo){
				$userinfo=array(
					'user_nickname'=>lang('DELETE')
				);
			}

			$v['user_nickname']=$userinfo['user_nickname'];
	
			return $v;
			
		});
		
		//Paging--> Filter parameter
		$lists->appends($data);	

        // Get paging display
        $page = $lists->render();

    	$this->assign("lists",$lists);
    	$this->assign("page", $page);
		
    	return $this->fetch();

    }

    //sort
    public function listsorders() { 
        $ids = $_POST['listorders'];
		$ids = $this->request->param('listorders');
        foreach ($ids as $key => $r) {
            $data['orderno'] = $r;
            Db::name("video")->where(array('id' => $key))->update($data);
        }
				
        $status = true;
        if ($status) {

            $this->success(lang('SORT_UPDATE_SUCCESS'));
        } else {
			$this->error(lang('SORT_UPDATE_FAILED'));
        }
    }
}

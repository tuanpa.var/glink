<?php

/**
 * 坐骑管理
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class CarController extends AdminbaseController {

    function index(){

    	$lists = Db::name("car")
			->order("list_order asc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['thumb']=get_upload_path($v['thumb']);
            return $v;           
        });
        
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }
		
	function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('car')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        $action="删除坐骑：{$id}";
        setAdminLog($action);
                    
        $this->resetcache();
        $this->success(lang("DELETE_SUCCESS"));
            
	}
    //排序
    public function listOrder() { 
		
        $model = DB::name('car');
        parent::listOrders($model);
        
        $action="更新坐骑排序";
        setAdminLog($action);
        
        $this->resetcache();
        $this->success(lang('SORT_UPDATE_SUCCESS'));
        
    }

	function add(){
		return $this->fetch();
	}

	function addPost(){
		if ($this->request->isPost()) {
            
            $configpub=getConfigPub();
            
            $data = $this->request->param();
            
			$name=$data['name'];
			$name_en=$data['name_en'];

			if($name==""){
				$this->error(lang('PLEASE_FILL_IN_CHINESE_NAME'));
			}

			if($name_en==""){
				$this->error(lang('PLEASE_FILL_IN_ENGLISH_NAME'));
			}

			$needcoin=$data['needcoin'];
			if($needcoin==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED_MOUNT').$configpub['name_coin']);
			}

			if(!is_numeric($needcoin)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED_MOUNT').$configpub['name_coin']);
			}

			if($needcoin<1||$needcoin>99999999){
				$this->error(lang('MOUNT_REQUIRED').$configpub['name_coin']."必须在1-99999999之间");
			}

			if(floor($needcoin)!=$needcoin){
				$this->error(lang('MOUNT_REQUIRED').$configpub['name_coin'].lang('MUST_BE_INTEGER'));
			}
            
            $score=$data['score'];
			if($score==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED_MOUNT').$configpub['name_score']);
			}

			if(!is_numeric($score)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED_MOUNT').$configpub['name_score']);
			}

			if($score<1||$score>99999999){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED_MOUNT').$configpub['name_score']."必须在1-99999999之间");
			}

			if(floor($score)!=$score){
				$this->error(lang('MOUNT_REQUIRED').$configpub['name_score'].lang('MUST_BE_INTEGER'));
			}

			$thumb=$data['thumb'];
			if(!$thumb){
				$this->error(lang('PLEASE_UPLOAD_IMAGE'));
			}

			$data['thumb']=set_upload_path($thumb);

			$swf=$data['swf'];
			if(!$swf){
				$this->error(lang('UPLOAD_ANIMATION'));
			}

			$data['swf']=set_upload_path($swf);

			$swftime=$data['swftime'];
			if($swftime==""){
				$this->error(lang('PLEASE_FILL_IN_ANIMATION_DURATION'));
			}

			if(!is_numeric($swftime)){
				$this->error(lang('PLEASE_CONFIRM_ANIMATION_DURATION'));
			}

			if($swftime<0){
				$this->error(lang('ANIMATION_DURATION_CANNOT_BE_LESS_THAN_ZERO'));
			}

			$words=$data['words'];
			if($words==""){
				$this->error(lang('PLEASE_FILL_IN_THE_ENTRANCE_WORDS'));
			}
            $data['addtime']=time();
            
			$id = DB::name('car')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            
            $action="添加坐骑：{$id}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
            
		}
	}

	function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('car')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign('data', $data);
        return $this->fetch();
	}
	
	function editPost(){
		if ($this->request->isPost()) {
            
            $configpub=getConfigPub();
            $data = $this->request->param();
            
			$name=$data['name'];

			if($name==""){
				$this->error(lang('PLEASE_FILL_IN_MOUNT_NAME'));
			}
			$needcoin=$data['needcoin'];
			if($needcoin==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED_MOUNT').$configpub['name_coin']);
			}

			if(!is_numeric($needcoin)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED_MOUNT').$configpub['name_coin']);
			}

			if($needcoin<1||$needcoin>99999999){
				$this->error(lang('MOUNT_REQUIRED').$configpub['name_coin']."必须在1-99999999之间");
			}

			if(floor($needcoin)!=$needcoin){
				$this->error(lang('MOUNT_REQUIRED').$configpub['name_coin'].lang('MUST_BE_INTEGER'));
			}
            
            $score=$data['score'];
			if($score==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED_MOUNT').$configpub['name_score']);
			}

			if(!is_numeric($score)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED_MOUNT').$configpub['name_score']);
			}

			if($score<1||$score>99999999){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED_MOUNT').$configpub['name_score']."必须在1-99999999之间");
			}

			if(floor($score)!=$score){
				$this->error(lang('MOUNT_REQUIRED').$configpub['name_score'].lang('MUST_BE_INTEGER'));
			}

			$thumb=$data['thumb'];
			if(!$thumb){
				$this->error(lang('PLEASE_UPLOAD_IMAGE'));
			}

			$thumb_old=$data['thumb_old'];
			if($thumb!=$thumb_old){
				$data['thumb']=set_upload_path($thumb);
			}

			$swf=$data['swf'];
			if(!$swf){
				$this->error(lang('UPLOAD_ANIMATION'));
			}

			$swf_old=$data['swf_old'];
			if($swf!=$swf_old){
				$data['swf']=set_upload_path($swf);
			}

			$swftime=$data['swftime'];
			if($swftime==""){
				$this->error(lang('PLEASE_FILL_IN_ANIMATION_DURATION'));
			}

			if(!is_numeric($swftime)){
				$this->error(lang('PLEASE_CONFIRM_ANIMATION_DURATION'));
			}

			if($swftime<0){
				$this->error(lang('ANIMATION_DURATION_CANNOT_BE_LESS_THAN_ZERO'));
			}

			$words=$data['words'];
			if($words==""){
				$this->error(lang('PLEASE_FILL_IN_THE_ENTRANCE_WORDS'));
			}

			unset($data['thumb_old']);
			unset($data['swf_old']);
            
			$rs = DB::name('car')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            $action="修改坐骑：{$data['id']}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}
	}
    
    function resetcache(){
        $key='carinfo';

        $car_list=DB::name("car")->order("list_order asc")->select();
        if($car_list){
            setcaches($key,$car_list);
        }else{
			delcache($key);
		}
        return 1;
    }
}

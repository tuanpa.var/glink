<?php

/**
 * 提现
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class CashController extends AdminbaseController {

    protected function getStatus($k=''){
        $status=array(
            '0'=>lang('UNPROCESSED'),
            '1'=>lang('WITHDRAWAL_SUCCESS'),
            '2'=>lang('WITHDRAWAL_REJECTED'),
        );
        if($k===''){
            return $status;
        }
        
        return $status[$k] ?? '';
    }
    
    protected function getTypes($k=''){
        $type=array(
            '1'=>lang('ALIPAY'),
            '2'=>lang('WECHAT'),
            '3'=>lang('BANK_CARD'),
        );
        if($k===''){
            return $type;
        }
        
        return $type[$k] ?? '';
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
		
        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
            $cash['type']=1;
        }
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $uid= $data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['orderno|trade_no','like',"%".$keyword."%"];
        }
        
    	$lists = DB::name("cash_record")
            ->where($map)
            ->order('id desc')
            ->paginate(20);
        
        $lists->each(function($v,$k){
            $v['userinfo']=getUserInfo($v['uid']);
            return $v;
        });
        
        $lists->appends($data);
        $page = $lists->render();

        $cashrecord_total = DB::name("cash_record")->where($map)->sum("money");
        if($status=='')
        {
            $success=$map;
            $success[]=['status','=',1];
            $fail=$map;
            $fail[]=['status','=',2];
            $cashrecord_success = DB::name("cash_record")->where($success)->sum("money");
            $cashrecord_fail = DB::name("cash_record")->where($fail)->sum("money");
            $cash['success']=$cashrecord_success;
            $cash['fail']=$cashrecord_fail;
            $cash['type']=0;
        }
        $cash['total']=$cashrecord_total;
            
    	$this->assign('cash', $cash);
    	$this->assign('lists', $lists);
    	$this->assign('type', $this->getTypes());
    	$this->assign('status', $this->getStatus());
    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }
		
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        if($id){
            $result=DB::name("cash_record")->delete($id);				
            if($result){
                $action="删除提现记录：{$id}";
                setAdminLog($action);
                $this->success(lang('DELETE_SUCCESS'));
             }else{
                $this->error(lang('DELETE_FAILED'));
             }
        }else{				
            $this->error(lang('DATA_TRANSFER_FAILED'));
        }				
    }		

	function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('cash_record')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $data['userinfo']=getUserInfo($data['uid']);
        
        $this->assign('type', $this->getTypes());
        $this->assign('status', $this->getStatus());
        $this->assign('data', $data);
        return $this->fetch();
	}
    
    function editPost(){
		if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
			$status=$data['status'];
			$uid=$data['uid'];
			$votes=$data['votes'];
			$id=$data['id'];

			if($status=='0'){
				$this->success(lang("MODIFICATION_SUCCESSFUL"));
			}

            $data['uptime']=time();
            
			$rs = DB::name('cash_record')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            if($status=='2'){
                 DB::name("user")->where(["id"=>$uid])->inc("votes",$votes)->update();
                $action="修改提现记录：{$id} - 拒绝";
            }else if($status=='1'){
                $action="修改提现记录：{$id} - 同意";
            }
            
            setAdminLog($action);
            
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}
	}
    
    function export(){

        $data = $this->request->param();
        $map=[];
		
        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
            $cash['type']=1;
        }
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        $uid=$data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['orderno|trade_no','like',"%".$keyword."%"];
        }
        
        $xlsName  = lang('WITHDRAWAL');
        
        $xlsData=DB::name("cash_record")
            ->where($map)
            ->order('id desc')
            ->select()
            ->toArray();

        if(empty($xlsData)){
            $this->error(lang("DATA_EMPTY"));
        }

        foreach ($xlsData as $k => $v)
        {
            $userinfo=getUserInfo($v['uid']);
            $xlsData[$k]['user_nickname']= $userinfo['user_nickname']."(".$v['uid'].")";
            $xlsData[$k]['addtime']=date("Y-m-d H:i:s",$v['addtime']); 
            $xlsData[$k]['uptime']=date("Y-m-d H:i:s",$v['uptime']); 
            $xlsData[$k]['status']=$this->getStatus($v['status']);
        }


        $action="导出提现记录：".DB::name("cash_record")->getLastSql();
        setAdminLog($action);
        $cellName = array('A','B','C','D','E','F','G','H');
        $xlsCell  = array(
            array('id', lang('SERIAL_NUMBER')),
            array('user_nickname', lang('ANCHOR_NAME')),
            array('votes', lang('EXCHANGE_VOTES')),
            array('money', lang('WITHDRAWAL_AMOUNT')),
            array('trade_no', lang('THIRD_PARTY_PAYMENT_ORDER_NUMBER')),
            array('status', lang('STATUS')),
            array('addtime', lang('SUBMISSION_TIME')),
            array('uptime', lang('PROCESSING_TIME')),
        );
        exportExcel($xlsName,$xlsCell,$xlsData,$cellName);
    }
    
}

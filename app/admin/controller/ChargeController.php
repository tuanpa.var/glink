<?php

/**
 * 充值记录
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class ChargeController extends AdminbaseController {
    protected function getStatus($k=''){
        $status=array(
            '0'=>lang('UNPAID'),
            '1'=>lang('COMPLETE'),
        );
        if($k===''){
            return $status;
        }
        
        return $status[$k] ?? '';
    }
    
    protected function getTypes($k=''){
        $type=array(
            '1'=>lang('ALIPAY'),
            '2'=>lang('WECHAT'),
            '3'=>lang('APPLE_PAY'),
            '4'=>lang('WECHAT_MINI_PROGRAM'),
            '5'=>lang('PAYPAL'),
            '6'=>lang('BRAIN_TREE_PAYPAL'),
        );
        if($k===''){
            return $type;
        }
        
        return $type[$k] ?? '';
    }
    
    protected function getAmbient($k=''){
        $ambient=array(
            "1"=>array(
                '0'=>'App',
                '1'=>'PC',
                '2'=>'H5',
            ),
            "2"=>array(
                '0'=>'App',
                '1'=>lang('PUBLIC_ACCOUNT'),
                '2'=>'PC',
                '3'=>'H5',
            ),
            "3"=>array(
                '0'=>lang('SANDBOX'),
                '1'=>lang('PRODUCTION'),
            ),
            "4"=>array(
                '0'=>'App',
                '1'=>'PC',
            ),
            "5"=>array(
                '0'=>lang('SANDBOX'),
                '1'=>lang('PRODUCTION'),
            ),
            "6"=>array(
                '0'=>lang('SANDBOX'),
                '1'=>lang('PRODUCTION'),
            ),
        );
        
        if($k===''){
            return $ambient;
        }
        
        return $ambient[$k] ?? '';
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';

        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }

        $uid= $data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }

        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['orderno|trade_no','like','%'.$keyword.'%'];
        }


        $lists = Db::name("charge_user")
            ->where($map)
			->order("id desc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
            if($v['giftid']){
                $gift_info=Db::name("gift")
                    ->field("gifticon,giftname")
                    ->where(['mark'=>1,'id'=>$v['giftid']])
                    ->find();
                if(!$gift_info){
                    $gift_info=['gifticon'=>'','giftname'=>''];
                }
            }else{
                $gift_info=['gifticon'=>'','giftname'=>''];
            }
            $v['gift_info']=$gift_info;
            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
        $this->assign('status', $this->getStatus());
        $this->assign('type', $this->getTypes());
        $this->assign('ambient', $this->getAmbient());
    	
        $moneysum = Db::name("charge_user")
            ->where($map)
			->sum('money');
        if(!$moneysum){
            $moneysum=0;
        }

    	$this->assign('moneysum', $moneysum);
        
    	return $this->fetch();
    }
    
    function setPay(){
        $id = $this->request->param('id', 0, 'intval');
        if($id){
            $result=Db::name("charge_user")->where(["id"=>$id,"status"=>0])->find();                
            if($result){

                $uid=$result['touid'];
                
                /* 更新会员虚拟币 */
                $coin=$result['coin']+$result['coin_give'];
                Db::name("user")->where("id='{$uid}'")->inc("coin",$coin)->update();


                /* 更新 订单状态 */
                Db::name("charge_user")->where("id='{$result['id']}'")->update(array("status"=>1));

                if($result['is_first']==1){

                    $now=time();


                    if($result['score'] >0){
                        Db::name("user")->where("id='{$uid}'")->inc("score",$result['score'])->update();

                        $arr=array(
                            'type'=>1,
                            'action'=>'22',
                            'uid'=>$result['uid'],
                            'touid'=>$uid,
                            'giftid'=>$result['id'],
                            'giftcount'=>1,
                            'totalcoin'=>$result['score'],
                            'addtime'=>$now
                        );

                        Db::name("user_scorerecord")->insert($arr);
                    }

                    if($result['vip_length']>0){
                        $endtime=60*60*24*$result['vip_length'];
                        $vip_info=Db::name("vip_user")->where("uid={$uid}")->find();
                        if(!$vip_info){
                            $endtime=$endtime+$now;
                            Db::name("vip_user")->insert(
                                [
                                    'uid'=>$uid,
                                    'addtime'=>$now,
                                    'endtime'=>$endtime
                                ]
                            );
                        }else{
                            if($vip_info['endtime']>$now){
                                $endtime=$endtime+$vip_info['endtime'];
                            }else{
                                $endtime=$endtime+$now;
                            }

                            Db::name('vip_user')->where(["uid"=>$uid])
                                ->update(['endtime'=>$endtime]);
                        }

                        $key='vip_'.$uid;
                        $isexist=Db::name("vip_user")->where(["uid"=>$uid])->find();
                        if($isexist){
                            setcaches($key,$isexist);
                        }
                    }


                    if($result['giftid']>0 && $result['gift_num']>0){
                        $backpack_info=Db::name("backpack")
                            ->where(['uid'=>$uid,'giftid'=>$result['giftid']])
                            ->find();

                        if(!$backpack_info){
                            $arr=array(
                                'uid'=>$uid,
                                'giftid'=>$result['giftid'],
                                'nums'=>$result['gift_num']
                            );

                            Db::name("backpack")->insert($arr);
                        }else{
                            Db::name("backpack")
                                ->where(['uid'=>$uid,'giftid'=>$result['giftid']])
                                ->inc("nums",$result['gift_num'])
                                ->update();
                        }
                    }


                    Db::name("user")->where(['id'=>$uid])->update(array('firstcharge_used'=>1));


                }


   
                $action="确认充值：{$id}";
                setAdminLog($action);
                $this->success(lang('OPERATION_SUCCESS'));
             }else{
                $this->error(lang('DATA_TRANSFER_FAILED'));
             }          
        }else{              
            $this->error(lang('DATA_TRANSFER_FAILED'));
        }                                         
    }
    
    
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('charge_user')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        $action="删除充值记录：{$id}";
        setAdminLog($action);
                    
        $this->success(lang("DELETE_SUCCESS"));
        							  			
    }

    function export(){
    
        $data = $this->request->param();
        $map=[];
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';

        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        $status=$data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }

        $uid=$data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }

        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['orderno|trade_no','like','%'.$keyword.'%'];
        }


        $xlsName  = lang('RECHARGE_RECORD');

        $xlsData=Db::name("charge_user")
            ->field('id,uid,money,coin,coin_give,orderno,type,trade_no,status,addtime')
            ->where($map)
            ->order('id desc')
			->select()
            ->toArray();

        if(empty($xlsData)){
            $this->error(lang("DATA_EMPTY"));
        }
        
        foreach ($xlsData as $k => $v) {
            $userinfo=getUserInfo($v['uid']);
            $xlsData[$k]['user_nickname']= $userinfo['user_nickname']."(".$v['uid'].")";
            $xlsData[$k]['addtime']=date("Y-m-d H:i:s",$v['addtime']); 
            $xlsData[$k]['type']=$this->getTypes($v['type']);
            $xlsData[$k]['status']=$this->getStatus($v['status']);
        }

        $action="导出充值记录：".Db::name("charge_user")->getLastSql();
        setAdminLog($action);
        
        $cellName = array('A','B','C','D','E','F','G','H','I','J');
        $xlsCell  = array(
            array('id', lang('SERIAL_NUMBER')),
            array('user_nickname', lang('MEMBER')),
            array('money',lang('RMB_AMOUNT')),
            array('coin',lang('EXCHANGE_POINTS')),
            array('coin_give', lang('GIFT_POINTS')),
            array('orderno', lang('MERCHANT_ORDER_NUMBER')),
            array('type', lang('PAYMENT_TYPE')),
            array('trade_no', lang('THIRD_PARTY_PAYMENT_ORDER_NUMBER')),
            array('status', lang('ORDER_STATUS')),
            array('addtime', lang('SUBMISSION_TIME'))
        );
        exportExcel($xlsName,$xlsCell,$xlsData,$cellName);
    }

}

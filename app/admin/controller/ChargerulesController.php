<?php

/**
 * Recharge rules
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class ChargerulesController extends AdminbaseController {

		
    function index(){
        
        $lists = Db::name("charge_rules")
            ->where(['type'=>'0'])
			->order("list_order asc")
			->paginate(20);
        
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
    	
    	return $this->fetch();
        
    }		
		
	function del(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('charge_rules')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        $action="删除充值规则：{$id}";
        setAdminLog($action);
                    
        $this->resetcache();
        $this->success(lang('DELETE_SUCCESS'),url("Chargerules/index"));			
	}
    
    //sort
    public function listOrder() { 
		
        $model = DB::name('charge_rules');
        parent::listOrders($model);
        
        $action="更新充值规则排序";
        setAdminLog($action);
        
        $this->resetcache();
        $this->success(lang('SORT_UPDATE_SUCCESS'));
        
    }	

	
    function add(){
        $configpub=getConfigPub();
        $this->assign('name_coin',$configpub['name_coin']);
		return $this->fetch();
    }	
	
    function addPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            $configpub=getConfigPub();

            $name=$data['name'];
            $money=$data['money'];
            $coin=$data['coin'];
            $coin_ios=$data['coin_ios'];
            $product_id=$data['product_id'];
            $give=$data['give'];
            $coin_paypal=$data['coin_paypal'];

            if(!$name){
                $this->error(lang('PLEASE_FILL_IN_NAME'));
            }

            if(!$money){
                $this->error(lang('PLEASE_FILL_IN_THE_PRICE'));
            }

            if(!is_numeric($money)){
                $this->error(lang('PRICE_MUST_BE_NUMERIC'));
            }

            if($money<=0||$money>99999999){
                $this->error(lang('PRICE_MUST_BE_BETWEEN_001_AND_99999999'));
            }

            $data['money']=round($money,2);

            if(!$coin){
                $this->error(lang('PLEASE_FILL_IN_THE').$configpub['name_coin']);
            }

            if(!is_numeric($coin)){
                $this->error($configpub['name_coin'].lang('MUST_BE_NUMERIC'));
            }

            if($coin<1||$coin>99999999){
                $this->error($configpub['name_coin'].lang('BETWEEN_1_AND_99999999'));
            }

            if(floor($coin)!=$coin){
                $this->error($configpub['name_coin'].lang('MUST_BE_INTEGER'));
            }

            if(!$coin_ios){
                $this->error(lang('PLEASE_FILL_IN_APPLE_PAY').$configpub['name_coin']);
            }

            if(!is_numeric($coin_ios)){
                $this->error(lang('APPLE_PAY').$configpub['name_coin'].lang('MUST_BE_NUMERIC'));
            }

            if($coin_ios<1||$coin_ios>99999999){
                $this->error(lang('APPLE_PAY').$configpub['name_coin'].lang('BETWEEN_1_AND_99999999'));
            }

            if(floor($coin_ios)!=$coin_ios){
                $this->error(lang('APPLE_PAY').$configpub['name_coin'].lang('MUST_BE_INTEGER'));
            }

            if($product_id==''){
                $this->error(lang('APPLE_PROJECT_ID_CANNOT_BE_EMPTY'));
            }

            if($give==''){
               $this->error(lang('GIVEAWAY').$configpub['name_coin'].lang('CAN_NOT_BE_EMPTY')); 
            }

            if(!is_numeric($give)){
                $this->error(lang('GIVEAWAY').$configpub['name_coin'].lang('MUST_BE_NUMERIC')); 
            }

            if($give<0||$give>99999999){
                $this->error(lang('GIVEAWAY').$configpub['name_coin']."在0-99999999之间"); 
            }

            if(floor($give)!=$give){
                $this->error(lang('GIVEAWAY').$configpub['name_coin'].lang('MUST_BE_INTEGER')); 
            }

            if($coin_paypal==''){
               $this->error(lang('PAYPAL_PAYMENT').$configpub['name_coin'].lang('CAN_NOT_BE_EMPTY'));
            }

            if(!is_numeric($coin_paypal)){
                $this->error(lang('PAYPAL_PAYMENT').$configpub['name_coin'].lang('MUST_BE_NUMERIC'));
            }

            if($coin_paypal<1||$coin_paypal>99999999){
                $this->error(lang('PAYPAL_PAYMENT').$configpub['name_coin'].lang('BETWEEN_1_AND_99999999'));
            }

            if(floor($coin_paypal)!=$coin_paypal){
                $this->error(lang('PAYPAL_PAYMENT').$configpub['name_coin'].lang('MUST_BE_INTEGER'));
            }
            
            $data['addtime']=time();
            
			$id = DB::name('charge_rules')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            
            $action="添加充值规则：{$id}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
            
		}
	}
    
    function edit(){
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('charge_rules')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }

        $configpub=getConfigPub();
        $this->assign('name_coin',$configpub['name_coin']);
        $this->assign('name_score',$configpub['name_score']);
        if($data['type']==1){
            $hotGiftLists=Db::name("gift")
                ->field("id,giftname,gifticon")
                ->where("mark=1")
                ->order("list_order")
                ->select()
                ->toArray(); //Popular gifts
            $this->assign('hotGiftLists',$hotGiftLists);
        }
        
        $this->assign('data', $data);
        return $this->fetch();
        
    }
	
    function editPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();

            $configpub=getConfigPub();

            $name=$data['name'];
            $name_en=$data['name_en'];
            $money=$data['money'];
            $coin=$data['coin'];
            $product_id=$data['product_id'];
            $type=$data['type'];


            if(!$name){
                $this->error(lang('PLEASE_FILL_IN_NAME'));
            }

            if(!$money){
                $this->error(lang('PLEASE_FILL_IN_THE_PRICE'));
            }

            if(!is_numeric($money)){
                $this->error(lang('PRICE_MUST_BE_NUMERIC'));
            }

            if($money<=0||$money>99999999){
                $this->error(lang('PRICE_MUST_BE_BETWEEN_001_AND_99999999'));
            }

            $data['money']=round($money,2);

            if(!$coin){
                $this->error(lang('PLEASE_FILL_IN_THE').$configpub['name_coin']);
            }

            if(!is_numeric($coin)){
                $this->error($configpub['name_coin'].lang('MUST_BE_NUMERIC'));
            }

            if($coin<1||$coin>99999999){
                $this->error($configpub['name_coin'].lang('BETWEEN_1_AND_99999999'));
            }

            if(floor($coin)!=$coin){
                $this->error($configpub['name_coin'].lang('MUST_BE_INTEGER'));
            }

            if($product_id==''){
                $this->error(lang('APPLE_PROJECT_ID_CANNOT_BE_EMPTY'));
            }

            
            if($type==0){ //Ordinary recharge rules
                $give=$data['give'];
                $coin_paypal=$data['coin_paypal'];
                $coin_ios=$data['coin_ios'];

                //-----------------
                if($give==''){
                   $this->error(lang('GIVEAWAY').$configpub['name_coin'].lang('CAN_NOT_BE_EMPTY')); 
                }

                if(!is_numeric($give)){
                    $this->error(lang('GIVEAWAY').$configpub['name_coin'].lang('MUST_BE_NUMERIC')); 
                }

                if($give<0||$give>99999999){
                    $this->error(lang('GIVEAWAY').$configpub['name_coin']."在0-99999999之间"); 
                }

                if(floor($give)!=$give){
                    $this->error(lang('GIVEAWAY').$configpub['name_coin'].lang('MUST_BE_INTEGER')); 
                }

                //-----------------
                if($coin_paypal==''){
                   $this->error(lang('PAYPAL_PAYMENT').$configpub['name_coin'].lang('CAN_NOT_BE_EMPTY'));
                }

                if(!is_numeric($coin_paypal)){
                    $this->error(lang('PAYPAL_PAYMENT').$configpub['name_coin'].lang('MUST_BE_NUMERIC'));
                }

                if($coin_paypal<1||$coin_paypal>99999999){
                    $this->error(lang('PAYPAL_PAYMENT').$configpub['name_coin'].lang('BETWEEN_1_AND_99999999'));
                }

                if(floor($coin_paypal)!=$coin_paypal){
                    $this->error(lang('PAYPAL_PAYMENT').$configpub['name_coin'].lang('MUST_BE_INTEGER'));
                }

                //-----------------
                if(!$coin_ios){
                    $this->error(lang('PLEASE_FILL_IN_APPLE_PAY').$configpub['name_coin']);
                }

                if(!is_numeric($coin_ios)){
                    $this->error(lang('APPLE_PAY').$configpub['name_coin'].lang('MUST_BE_NUMERIC'));
                }

                if($coin_ios<1||$coin_ios>99999999){
                    $this->error(lang('APPLE_PAY').$configpub['name_coin'].lang('BETWEEN_1_AND_99999999'));
                }

                if(floor($coin_ios)!=$coin_ios){
                    $this->error(lang('APPLE_PAY').$configpub['name_coin'].lang('MUST_BE_INTEGER'));
                }


            }else{

                $score=$data['score'];
                $vip_length=$data['vip_length'];
                $giftid=$data['giftid'];
                $gift_num=$data['gift_num'];

                //-----------
                if($score==''){
                   $this->error($configpub['name_score'].lang('CAN_NOT_BE_EMPTY'));
                }

                if(!is_numeric($score)){
                    $this->error($configpub['name_score'].lang('MUST_BE_NUMERIC'));
                }

                if($score<0||$score>99999999){
                    $this->error($configpub['name_score']."在0-99999999之间");
                }

                if(floor($score)!=$score){
                    $this->error($configpub['name_score'].lang('MUST_BE_INTEGER'));
                }

                //-----------
                if($vip_length==''){
                   $this->error(lang('GIFT_VIP_DURATION_CANNOT_BE_EMPTY'));
                }

                if(!is_numeric($vip_length)){
                    $this->error(lang('GIFT_VIP_DURATION_MUST_BE_NUMERIC'));
                }

                if($vip_length<0||$vip_length>99999999){
                    $this->error(lang('GIFT_VIP_DURATION_MUST_BE_BETWEEN_0_AND_99999999'));
                }

                if(floor($vip_length)!=$vip_length){
                    $this->error(lang('GIFT_VIP_DURATION_MUST_BE_INTEGER'));
                }

                //-------------
                if($giftid>0){
                    $gift_info=Db::name("gift")->where("id={$giftid} and mark=1")->find();
                   if(!$gift_info){
                        $this->error(lang('HOT_GIFT_NOT_EXIST'));
                   }

                   if($gift_num=='' || $gift_num<=0){
                       $this->error(lang('GIFT_COUNT_SHOULD_BE_POSITIVE_INTEGER'));
                    }
                }
               

               //-----------
                if($gift_num==''){
                   $this->error(lang('GIVE_HOT_GIFT_NUMBER_CANNOT_BE_EMPTY'));
                }

                if(!is_numeric($gift_num)){
                    $this->error(lang('GIVE_HOT_GIFT_NUMBER_MUST_BE_NUMERIC'));
                }

                if($gift_num<0||$gift_num>99999){
                    $this->error(lang('GIVE_HOT_GIFT_NUMBER_BETWEEN_0_AND_99999'));
                }

                if(floor($gift_num)!=$gift_num){
                    $this->error(lang('GIVE_VIP_DURATION_MUST_BE_INTEGER'));
                }

                if($giftid==0){
                    $data['gift_num']=0;
                }

               $data['coin_ios']=$coin;
               $data['coin_paypal']=$coin;
            }
            
            $data['uptime']=time();
            
			$rs = DB::name('charge_rules')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
			
            $action="修改充值规则：{$data['id']}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}
	}

    //Diamond first charge rules
    function firstcharge(){
        $lists = Db::name("charge_rules")
            ->where("type=1")
            ->order("list_order asc")
            ->paginate(20);

        $lists->each(function($v,$k){
            if($v['giftid']>0){
                $v['gift_info']=Db::name("gift")->where("id={$v['giftid']}")->find();  
            }else{
                $v['gift_info']=[];
            }
            
            return $v;           
        });
        
        $page = $lists->render();

        $this->assign('lists', $lists);

        $this->assign("page", $page);
        
        return $this->fetch();
    }
    	

    function resetcache(){
        $key='getChargeRules';
        $rules= DB::name("charge_rules")
            ->field('id,coin,coin_ios,money,product_id,give,coin_paypal')
            ->where("type=0")
            ->order('list_order asc')
            ->select();
        if($rules){
            setcaches($key,$rules);
        }else{
			delcache($key);
		}


        $key1='getFirstChargeRules';
        $first_rules= DB::name("charge_rules")
            ->field('id,name,name_en,coin,coin_ios,money,product_id,give,coin_paypal,score,vip_length,giftid,gift_num')
            ->where("type=1")
            ->order('list_order asc')
            ->select();
        if($first_rules){
            setcaches($key1,$first_rules);
        }else{
            delcache($key1);
        }
        return 1;
    }
}

<?php

/**
 * Consumption record
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class CoinrecordController extends AdminbaseController {
    
    protected function getTypes($k=''){
        $type=array(
            '0'=>lang('EXPENDITURE'),
            '1'=>lang('INCOME'),
        );
        if($k===''){
            return $type;
        }
        
        return $type[$k] ?? '';
    }
    
    protected function getAction($k=''){
        $action = [
            '1'  => lang('GIFT_GIVING'),
            '2'  => lang('BARRAGE'),
            '3'  => lang('LOGIN_REWARD'),
            '4'  => lang('VIP_PURCHASE'),
            '5'  => lang('MOUNT_PURCHASE'),
            '6'  => lang('ROOM_CHARGE'),
            '7'  => lang('TIME_CHARGE'),
            '8'  => lang('SEND_RED_PACKET'),
            '9'  => lang('GRAB_RED_PACKET'),
            '10' => lang('OPEN_GUARD'),
            '11' => lang('REGISTER_REWARD'),
            '12' => lang('GIFT_WINNING'),
            '13' => lang('JACKPOT_WINNING'),
            '14' => lang('DEPOSIT_PAYMENT'),
            '15' => lang('DEPOSIT_REFUND'),
            '16' => lang('TURNTABLE_GAME'),
            '17' => lang('TURNTABLE_WINNING'),
            '18' => lang('NICE_NUMBER_PURCHASE'),
            '19' => lang('GAME_BET'),
            '20' => lang('GAME_REFUND'),
            '21' => lang('DAILY_TASK'),
            '22' => lang('PLANET_BET'),
            '23' => lang('PLANET_DIAMOND_WINNING'),
            '24' => lang('LUCKY_TURNTABLE_BET'),
            '25' => lang('LUCKY_TURNTABLE_DIAMOND_WINNING'),
        ];
        if($k===''){
            return $action;
        }
        
        return $action[$k] ?? lang('UNKNOWN');
    }

    protected function getGame($k=''){
        $game=array(
            '1'=>lang('SMART_AND_BRAVE_THREE_CARDS'),
			'2'=>lang('PIRATE_CAPTAIN'),
			'3'=>lang('TURNTABLE'),
			'4'=>lang('HAPPY_COWBOY'),
			'5'=>lang('TWO_EIGHT_SHELLFISH'),
        );
        if($k===''){
            return $game;
        }
        
        return $game[$k] ?? '';
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        $type= $data['type'] ?? '';
        if($type!=''){
            $map[]=['type','=',$type];
        }
        
        $action= $data['action'] ?? '';
        if($action!=''){
            $map[]=['action','=',$action];
        }
        
        $uid= $data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $touid= $data['touid'] ?? '';
        if($touid!=''){
            $map[]=['touid','=',$touid];
        }
        
        $lists = Db::name("user_coinrecord")
            ->where($map)
			->order("id desc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
			$v['touserinfo']=getUserInfo($v['touid']);
            
            $action=$v['action'];
            if($action=='1'){
                $giftinfo=Db::name("gift")->field("giftname")->where("id='{$v['giftid']}'")->find();
                if(!$giftinfo){
                    $giftinfo['giftname']=lang('GIFT_DELETE');
                }
            }else if($action=='3'){
                $giftinfo['giftname']='第'.$v['giftid'].lang('DAY');
            }else if($action=='4'){
                $info=Db::name("vip")->field("name")->where("id='{$v['giftid']}'")->find();
                $giftinfo['giftname']=$info['name'];
            }else if($action=='5'){
                $info=Db::name("car")->field("name")->where("id='{$v['giftid']}'")->find();
                if(!$info){
                    $info['name']='坐骑已删除';
                }
                $giftinfo['giftname']=$info['name'];
            }else if($action=='18'){
                $info=Db::name("liang")->field("name")->where("id='{$v['giftid']}'")->find();
                if(!$info){
                    $info['name']='靓号已删除';
                }
                $giftinfo['giftname']=$info['name'];
            }else if($action=='10'){
                $info=Db::name("guard")->field("name")->where("id='{$v['giftid']}'")->find();
                $giftinfo['giftname']=$info['name'];
            }else if($action=='19' || $action=='20'){
                $info=Db::name("game")->field("action")->where("id='{$v['giftid']}'")->find();
                $giftinfo['giftname']=$this->getGame($info['action']);
            }else{
                $giftinfo['giftname']=$this->getAction($action);
                
            }
            $v['giftinfo']= $giftinfo;
                
            return $v;           
        });
    	
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
        $this->assign('action', $this->getAction());
        $this->assign('type', $this->getTypes());
        
    	return $this->fetch();
    
    }
		
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('user_coinrecord')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
                    
        $this->success(lang("DELETE_SUCCESS"));
        							  			
    }    

	//钻石消费记录
	function export(){

        $data = $this->request->param();
        $map=[];
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        $type= $data['type'] ?? '';
        if($type!=''){
            $map[]=['type','=',$type];
        }

        $action=$data['action'] ?? '';
        if($action!=''){
            $map[]=['action','=',$action];
        }

        $uid=$data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $touid= $data['touid'] ?? '';
        if($touid!=''){
            $map[]=['touid','=',$touid];
        }

        $xlsName  = "钻石消费记录";
		$lists = Db::name("user_coinrecord")
            ->where($map)
			->order("id desc")
			->select()
            ->toArray();
        
        if(empty($lists)){
            $this->error(lang("DATA_EMPTY"));
        }
      
		foreach($lists as $k=>$v){
			$userinfo=getUserInfo($v['uid']);
			$v['user_nickname']= $userinfo['user_nickname']."(".$v['uid'].")";
			
			$touserinfo=getUserInfo($v['touid']);
            $v['touser_nickname']= $touserinfo['user_nickname']."(".$v['touid'].")";
			
			
            $action=$v['action'];
            if($action=='1'){
                $giftinfo=Db::name("gift")->field("giftname")->where("id='{$v['giftid']}'")->find();
            }else if($action=='3'){
                $giftinfo['giftname']='第'.$v['giftid'].lang('DAY');
            }else if($action=='4'){
                $info=Db::name("vip")->field("name")->where("id='{$v['giftid']}'")->find();
                $giftinfo['giftname']=$info['name'];
            }else if($action=='5'){
                $info=Db::name("car")->field("name")->where("id='{$v['giftid']}'")->find();
                $giftinfo['giftname']=$info['name'];
            }else if($action=='18'){
                $info=Db::name("liang")->field("name")->where("id='{$v['giftid']}'")->find();
                $giftinfo['giftname']=$info['name'];
            }else if($action=='10'){
                $info=Db::name("guard")->field("name")->where("id='{$v['giftid']}'")->find();
                $giftinfo['giftname']=$info['name'];
            }else if($action=='19' || $action=='20'){
                $info=Db::name("game")->field("action")->where("id='{$v['giftid']}'")->find();
                $giftinfo['giftname']=$this->getGame($info['action']);
            }else{
                $giftinfo['giftname']=$this->getAction($action);
                
            }
    
            $v['giftname']= $giftinfo['giftname']."(".$v['giftid'].")";
           
            $v['type']= $this->getTypes($v['type']);
            $v['action']= $this->getAction($v['action']);
			$v['addtime']=date("Y-m-d H:i:s",$v['addtime']); 
             
            $lists[$k]=$v;     

		}

        $action="钻石消费记录：".Db::name("user_coinrecord")->getLastSql();
        setAdminLog($action);
        
        $cellName = array('A','B','C','D','E','F','G','H','I','J');
        $xlsCell  = array(
            array('id', lang('SERIAL_NUMBER')),
            array('type', lang('INCOME_EXPENSE_TYPE')),
            array('action', lang('INCOME_BEHAVIOR')),
            array('user_nickname', lang('MEMBER') . ' (ID)'),
            array('touser_nickname', lang('ANCHOR') . ' (ID)'),
            array('giftname', lang('ACTION_DESCRIPTION') . ' (ID)'),
            array('giftcount', lang('QUANTITY')),
            array('totalcoin', lang('TOTAL_PRICE')),
            array('showid', lang('LIVE_ID')),
            array('addtime', lang('TIME')),
        );
        exportExcel($xlsName,$xlsCell,$lists,$cellName);
    }
}

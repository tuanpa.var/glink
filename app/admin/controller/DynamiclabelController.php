<?php

/**
 * 动态话题标签
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use think\db\Query;


class DynamiclabelController extends AdminbaseController {

	//列表
    public function index(){

		$lists = Db::name('dynamic_label')
            ->where(function (Query $query) {
                $data = $this->request->param();
                $keyword= $data['keyword'] ?? '';
                if (!empty($keyword)) {
                    $query->where('name', 'like', "%$keyword%");
                }
            })
            ->order("orderno asc, isrecommend desc, use_nums desc")
            ->paginate(20);

	

        // Get paging display
        $page = $lists->render();
		
    	$this->assign('lists', $lists);
    	$this->assign("page", $page);

    	return $this->fetch();
    }
	
	//添加
    public function add(){ 
        return $this->fetch();				
    }
   
    public function addPost(){
        if($this->request->isPost()) {
			$data=$this->request->param();
			
            $name=$data['name'];
            $name_en=$data['name_en'];

            if($name==''){
                $this->error(lang('PLEASE_FILL_IN_THE_CHINESE_NAME'));
            }

            $isexist=Db::name('dynamic_label')
                ->where("name='{$name}'")
                ->find();
            if($isexist){
                $this->error(lang('THE_SAME_CHINESE_NAME_ALREADY_EXISTS'));
            }

            if($name_en==''){
                $this->error(lang('PLEASE_FILL_IN_THE_ENGLISH_NAME'));
            }

            $isexist=Db::name('dynamic_label')
                ->where(['name_en'=>$name_en])
                ->find();
            if($isexist){
                $this->error(lang('THE_SAME_ENGLISH_NAME_ALREADY_EXISTS'));
            }
            
            if($data['thumb']==''){
                $this->error(lang('PLEASE_UPLOAD_COVER'));
            }

            $result=Db::name('dynamic_label')->insertGetId($data); 
			
            if($result){
				$action="添加动态话题标签ID: ".$result;
				setAdminLog($action);
                $this->success(lang('ADD_SUCCESS'));
            }else{
				$this->error(lang('FAILED_TO_ADD'));
            }
        }			
    }		
	
	
	//编辑
    public function edit(){
        $id=$this->request->param('id');
        if($id){
            $data=Db::name('dynamic_label')->find($id);
            $this->assign('data', $data);						
        }else{				
            $this->error(lang('DATA_TRANSFER_FAILED'));
        }								  
        return $this->fetch();				
    }
    
    public function editPost(){
		
		 if($this->request->isPost()) {
			$data=$this->request->param();	
            $name=$data['name'];
            $name_en=$data['name_en'];
            if($name==''){
                $this->error(lang('PLEASE_FILL_IN_THE_CHINESE_NAME'));
            }
            
            $isexist=Db::name('dynamic_label')
				->where("name='{$name}' and id!={$data['id']}")
				->find();
            if($isexist){
                $this->error(lang('THE_SAME_CHINESE_NAME_ALREADY_EXISTS'));
            }

            if($name_en==''){
                $this->error(lang('PLEASE_FILL_IN_THE_ENGLISH_NAME'));
            }
            
            $isexist=Db::name('dynamic_label')
                ->where("id!={$data['id']}")
                ->where(['name_en'=>$name_en])
                ->find();
            if($isexist){
                $this->error(lang('THE_SAME_ENGLISH_NAME_ALREADY_EXISTS'));
            }
            
            if($data['thumb']==''){
                $this->error(lang('PLEASE_UPLOAD_COVER'));
            }
             
            $result=Db::name('dynamic_label')->update($data); 
            if($result!==false){
				$key='LabelInfo_'.$data['id'];
				delcache($key);
				$action="编辑动态话题标签ID: ".$data['id'];
				setAdminLog($action);
                $this->success(lang('UPDATE_SUCCESS'));
            }else{
                $this->error(lang('FAILED_TO_EDIT'));
            }
        }			
			
    }

    //删除
    public function del(){
        $id=$this->request->param('id');
        if($id){
            $result=Db::name('dynamic_label')->delete($id);
            if($result){

                $action="删除动态话题标签ID：{$id}";
                setAdminLog($action);
                $this->success(lang('DELETE_SUCCESS'));
            }else{
                $this->error(lang('DELETE_FAILED'));
            }
        }else{
            $this->error(lang('DATA_TRANSFER_FAILED'));
        }
        return $this->fetch();
    }

    //排序
    public function listsorders() {

        $ids=$this->request->param('listsorders');
        foreach ($ids as $key => $r) {
            $data['orderno'] = $r;
            Db::name('dynamic_label')->where(array('id' => $key))->update($data);
        }

        $status = true;
        if ($status) {

            $action="更新动态话题标签排序";
            setAdminLog($action);
            $this->success(lang('SORT_UPDATE_SUCCESS'));
        } else {
            $this->error(lang('SORT_UPDATE_FAILED'));
        }
    }

    /*是否推荐*/
    public function setrecommend(){

        $data=$this->request->param();	
		$id=$data['id'];
		$isrecommend=$data['isrecommend'];
        if ($id) {
            Db::name("dynamic_label")->where(["id" => $id])->update(['isrecommend'=>$isrecommend]);

			$isrecommend_name=$isrecommend==1?'推荐':'取消推荐';
			$action=$isrecommend_name."动态话题标签ID: ".$id;
			setAdminLog($action);
            $this->success(lang('OPERATION_SUCCESSFUL'));
        } else {
            $this->error(lang('DATA_TRANSFER_FAILED'));
        
		}   
	}
}

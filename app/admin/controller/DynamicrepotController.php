<?php

/* 动态举报 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class DynamicrepotController extends AdminBaseController
{

    protected function getStatus($k=''){
        $status=array(
            "0"=>lang('UNDER_REVIEW'),
            "1"=>lang('PROCESSED'),
        );

        if($k===''){
            return $status;
        }
        return $status[$k] ?? '';
    }
    
    
    public function index(){
        
        $data = $this->request->param();
        $map=[];
        
        $status= $data['status'] ?? '';
        
        if($status!=''){
            $map[]=['status','=',$status];
        }

        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $uid= $data['uid'] ?? '';
        if($uid!=''){
            $map[]=['uid','=',$uid];
            $lianguid=getLianguser($uid);
            if($lianguid){
                //$map[]=['uid',['=',$uid],['in',$lianguid],'or'];
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $touid= $data['touid'] ?? '';
        if($touid!=''){
            $lianguid=getLianguser($touid);
            if($lianguid){
                //$map[]=['touid',['=',$touid],['in',$lianguid],'or'];
                array_push($lianguid,$touid);
                $map[]=['touid','in',$lianguid];
            }else{
                $map[]=['touid','=',$touid];
            }
        }
        
        $dynamicid= $data['dynamicid'] ?? '';
        if($dynamicid!=''){
            $map[]=['dynamicid','=',$dynamicid];
        }
        
        
        $list = Db::name('dynamic_report')
            ->where($map)
            ->order("id desc")
            ->paginate(20);
        
        $list->each(function($v,$k){
           $v['userinfo']= getUserInfo($v['uid']);
           $v['touserinfo']= getUserInfo($v['touid']);
           //获取动态是否下架
           $isdel=Db::name("dynamic")->where("id={$v['dynamicid']}")->value("isdel");
           isset($isdel)?$v['isdel']=$isdel:$v['isdel']=0;
           return $v; 
        });
        
        $list->appends($data);
        
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
        $this->assign('status', $this->getStatus());

        return $this->fetch();
    }


    public function setstatus()
    {
        $id = $this->request->param('id', 0, 'intval');
        if(!$id){
            $this->error(lang('DATA_TRANSFER_FAILED'));
        }
        $status = $this->request->param('status', 0, 'intval');
        
        $rs=DB::name("dynamic_report")->where("id={$id}")->update(['status'=>$status]);
        if($rs===false){
            $this->error(lang('OPERATION_FAILED'));
        }

		$action="标记处理动态举报列表ID：{$id}";
        setAdminLog($action);
        
        $this->success(lang('OPERATION_SUCCESS'));        
    }
    
    public function del()
    {
        $id = $this->request->param('id', 0, 'intval');
        if(!$id){
            $this->error(lang('DATA_TRANSFER_FAILED'));
        }
        
        $result=DB::name("dynamic_report")->where("id={$id}")->delete();
        if(!$result){
            $this->error(lang("DELETE_FAILED"));
        }
		
		$action="删除动态举报列表ID：{$id}";
        setAdminLog($action);

        $this->success(lang("DELETE_SUCCESS"));
    }

}
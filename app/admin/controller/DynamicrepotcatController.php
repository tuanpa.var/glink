<?php

/* 动态举报 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class DynamicrepotcatController extends AdminBaseController
{

    public function index(){
        
        $list = Db::name('dynamic_report_classify')
            ->order("list_order asc")
            ->paginate(20);
        
        $page = $list->render();
        $this->assign("page", $page);
        $this->assign('list', $list);

        return $this->fetch();
    }


    public function add()
    {
        return $this->fetch();
    }

    public function addPost()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();
            
            $name=$data['name'];
            $name_en=$data['name_en'];
            
            if($name == ''){
                $this->error(lang('PLEASE_FILL_IN_THE_CHINESE_NAME'));
            }
            
            $map[]=['name','=',$name];
            $isexist = DB::name('dynamic_report_classify')->where($map)->find();
            if($isexist){
                $this->error(lang('THE_SAME_NAME_ALREADY_EXISTS'));
            }

            if($name_en == ''){
                $this->error(lang('PLEASE_FILL_IN_THE_ENGLISH_NAME'));
            }
            
            $map[]=['name_en','=',$name_en];
            $isexist = DB::name('dynamic_report_classify')->where($map)->find();
            if($isexist){
                $this->error(lang('THE_SAME_NAME_ALREADY_EXISTS'));
            }

            $id = DB::name('dynamic_report_classify')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
			$action="添加动态举报类型：{$id}";
			setAdminLog($action);
            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
        }
    }

    public function edit()
    {
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('dynamic_report_classify')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign('data', $data);
        return $this->fetch();
    }

    public function editPost()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $id=$data['id'];
            $name=$data['name'];
            $name_en=$data['name_en'];
            
            if($name == ''){
                $this->error(lang('PLEASE_FILL_IN_THE_CHINESE_NAME'));
            }
            
            $map[]=['name','=',$name];
            $map[]=['id','<>',$id];
            $isexist = DB::name('dynamic_report_classify')->where($map)->find();
            if($isexist){
                $this->error(lang('THE_SAME_NAME_ALREADY_EXISTS'));
            }

            if($name_en == ''){
                $this->error(lang('PLEASE_FILL_IN_THE_ENGLISH_NAME'));
            }
            
            $map[]=['name_en','=',$name_en];
            $map[]=['id','<>',$id];
            $isexist = DB::name('dynamic_report_classify')->where($map)->find();
            if($isexist){
                $this->error(lang('THE_SAME_NAME_ALREADY_EXISTS'));
            }

            $rs = DB::name('dynamic_report_classify')->update($data);

            if($rs === false){
                $this->error(lang('EDIT_FAILED'));
            }
			
			$action="修改动态举报类型：{$id}";
			setAdminLog($action);
            $this->resetcache();
            $this->success(lang('EDIT_SUCCESS'));
        }
    }
    
    public function listOrder()
    {
        $model = DB::name('dynamic_report_classify');
        parent::listOrders($model);
        $this->resetcache();
		
		
		$action="更新动态举报类型排序";
		setAdminLog($action);
        $this->success(lang('SORT_UPDATE_SUCCESS'));
    }

    public function del()
    {
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('dynamic_report_classify')->where('id',$id)->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
		
		
		$action="删除动态举报类型：{$id}";
        setAdminLog($action);
        $this->resetcache();
        $this->success(lang("DELETE_SUCCESS"));
    }


    protected function resetcache(){
        $key='getDynamicreportClass';

        $list=DB::name('dynamic_report_classify')
                ->order("list_order asc,id desc")
                ->select();
        if($list){
            setcaches($key,$list);
        }else{
			delcache($key);
		}
    }
}
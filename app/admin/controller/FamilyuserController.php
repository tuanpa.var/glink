<?php

/**
 * 家族成员
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class FamilyuserController extends AdminbaseController {
    
    protected function getState($k=''){
        $status=array(
            '0'=>lang('UNAUDITED'),
            '1'=>lang('AUDIT_FAILED'),
            '2'=>lang('APPROVED'),
        );
        if($k===''){
            return $status;
        }
        
        return $status[$k] ?? '';
    }

    protected function getApplyStatus($k=''){
        $status=array(
            '0'=>lang('WAITING_FOR_REVIEW'),
            '1'=>lang('APPROVED'),
            '-1'=>lang('REJECTED'),
        );
        if($k===''){
            return $status;
        }
        
        return $status[$k] ?? '';
    }
    
    protected function getFamily($k=''){
        $list = Db::name('family')
            ->where('state=2')
            ->order("id desc")
            ->column('*','id');
        
        if($k===''){
            return $list;
        }
        
        return $list[$k] ?? '';
    }

	function index(){
        $data = $this->request->param();
        $map=[];
        $map[]=['state','<>',3];
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $state= $data['state'] ?? '';
        if($state!=''){
            $map[]=['state','=',$state];
        }

        $familyid=$data['familyid'] ?? '';
        if($familyid!=''){
            $map[]=['familyid','=',$familyid];
        }
        
        $uid= $data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
			
        $this->family=$this->getFamily();
        
    	$lists = Db::name("family_user")
                ->where($map)
                ->order("id DESC")
                ->paginate(20);
        
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
			$v['family']=$this->family[$v['familyid']];
            return $v;
        });
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	$this->assign("state", $this->getState());
    	
    	return $this->fetch();
	}
	
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        $rs = DB::name('family_user')->where("id={$id}")->find();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        $data=array(
            'state'=>3,
            'signout'=>3,
            'signout_istip'=>3,
        );
            
        DB::name("family_user")->where(["id"=>$id])->update($data);	
        
        $action="删除家族成员：{$id}";
        setAdminLog($action);
        
        $this->success(lang("DELETE_SUCCESS"));
	}
	
	function add(){
		return $this->fetch();
	}

	function addPost(){
        if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$uid=$data['uid'];

			if($uid==""){
                $this->error(lang('PLEASE_FILL_IN_USER_ID'));
			}
            
            $isexist=DB::name('user')->where(["id"=>$uid,"user_type"=>2])->value('id');
            if(!$isexist){
                $this->error(lang('USER_DOES_NOT_EXIST'));
            }
            
            $isfamily=DB::name('family')->where(["uid"=>$uid])->find();
            if($isfamily){
                $this->error(lang('USER_ALREADY_FAMILY_HEAD'));
            }
            
            $isexist=DB::name('family_user')->where(["uid"=>$uid])->find();
            if($isexist && $isexist['state']==2){
                $this->error(lang('THIS_USER_HAS_APPLIED_FOR_FAMILY'));
            }

			$familyid=$data['familyid'];
			if($familyid==""){
                $this->error(lang('PLEASE_FILL_IN_THE_FAMILY_ID'));
			}
            $family=DB::name("family")->where(["id"=>$familyid])->find();
            if(!$family){
                $this->error(lang('THE_FAMILY_DOES_NOT_EXIST'));
            }
			
            if($family['state']!=2){
                $this->error(lang('THIS_FAMILY_HAS_NOT_BEEN_APPROVED'));
            }
            
            $data['state']=2;
            $data['addtime']=time();
            $data['uptime']=time();
            
            if($isexist){
                $id = DB::name('family_user')->where(['uid'=>$uid])->update($data);
            }else{
                $id = DB::name('family_user')->insertGetId($data);
            }
            
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            
            $action="添加家族成员：{$uid}";
            setAdminLog($action);
            
            $this->success(lang('ADD_SUCCESS'));
            
		}
	
	}

    function edit(){

        $id  = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('family_user')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $userinfo=getUserInfo($data['uid']);
        
        $family=Db::name("family")->field("name,divide_family")->where(["id"=>$data['familyid']])->find();
        
        $this->assign('data', $data);
        $this->assign('family', $family);
        $this->assign('userinfo', $userinfo);
        $this->assign('state', $this->getState());
        return $this->fetch();				

	}

	function editPost(){

        if ($this->request->isPost()) {
            
            $data  = $this->request->param();
            $data['uptime']=time();
			$rs = DB::name('family_user')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            $action="修改家族成员信息：{$data['uid']}";
            setAdminLog($action);
            
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}
        	
	}

     //家族成员分成申请
    function divideapply(){
       $data = $this->request->param();
        $map=[];
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }
        
        $familyid= $data['familyid'] ?? '';
        if($familyid!=''){
            $map[]=['familyid','=',$familyid];
        }

        $uid=$data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
            
        $this->family=$this->getFamily();
        
        $lists = Db::name("family_user_divide_apply")
                ->where($map)
                ->order("addtime DESC")
                ->paginate(20);

        $lists->each(function($v,$k){
            $v['userinfo']=getUserInfo($v['uid']);
            $v['family']=$this->getFamily($v['familyid']);
            return $v;
        });

        
        $lists->appends($data);
        $page = $lists->render();

        $this->assign('lists', $lists);
        $this->assign("page", $page);
        $this->assign("status", $this->getApplyStatus());
        
        return $this->fetch(); 
    }

    //分成申请处理
    function applyedit(){
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('family_user_divide_apply')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $userinfo=getUserInfo($data['uid']);
        
        $family=Db::name("family")->field("name,divide_family")->where(["id"=>$data['familyid']])->find();
        
        $this->assign('data', $data);
        $this->assign('family', $family);
        $this->assign('userinfo', $userinfo);
        $this->assign('status', $this->getApplyStatus());
        return $this->fetch();
    }

    //家族成员分成申请修改提交
    function applyeditPost(){

        if ($this->request->isPost()) {

            //获取后台的审核开关
            $configpri=getConfigPri();
            $family_member_divide_switch=$configpri['family_member_divide_switch'];

            if(!$family_member_divide_switch){
                $this->error(lang('MEMBER_DIVIDE_SETTING_CLOSED'));
            }
            
            $data = $this->request->param();
            $divide=$data['divide'];
            $status=$data['status'];

            if(!is_numeric($divide)){
                $this->error(lang('DIVIDE_MUST_BE_NUMERIC'));
            }

            if($divide<0||$divide>100){
                $this->error(lang('DIVIDE_BETWEEN_0_100'));
            } 

            if(floor($divide)!=$divide){
                $this->error(lang('DIVIDE_MUST_BE_INTEGER'));
            }
            
            $data['uptime']=time();
            
            $rs = DB::name('family_user_divide_apply')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }

            $action="修改家族成员分成比例：{$data['uid']}";

            if($status==1){ //审核通过

                //修改家族成员分成比例
                $data1=array(
                    'uptime'=>time(),
                    'divide_family'=>$divide
                );

                Db::name("family_user")->where("uid={$data['uid']} and familyid={$data['familyid']}")->update($data1);

                $action.=',成功';
            }else if($status==-1){
                $action.=',拒绝';
            }else{
                $action.=',等待审核';
            }

            setAdminLog($action);
            
            $this->success(lang('EDIT_SUCCESS'), 'Familyuser/divideapply');
        }
    }

    //家族分成删除
    function delapply(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('family_user_divide_apply')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }

        $action="删除家族成员分成申请：{$id}";
        setAdminLog($action);
        
        $this->success(lang("DELETE_SUCCESS"));
    }
    
}
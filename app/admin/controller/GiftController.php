<?php

/**
 * 礼物
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class GiftController extends AdminbaseController {
    protected function getTypes($k=''){
        $type=[
            '0'=>lang('REGULAR_GIFT'),
            '1'=>lang('LUXURY_GIFT'),
			'3'=>lang('HAND_DRAWN_GIFT'),
        ];
        if($k===''){
            return $type;
        }
        return $type[$k] ?? '';
    }
    protected function getMark($k=''){
        $mark=[
            '0'=>lang('ORDINARY'),
            '1'=>lang('POPULAR'),
            '2'=>lang('GUARDIAN'),
            '3'=>lang('LUCKY'),
        ];
        if($k===''){
            return $mark;
        }
        return $mark[$k] ?? '';
    }
    
    protected function getSwftype($k=''){
        $swftype=[
            '0'=>'GIF',
            '1'=>'SVGA',
        ];
        if($k===''){
            return $swftype;
        }
        return $swftype[$k] ?? '';
    }
    
    function index(){

    	$lists = Db::name("gift")
            ->where('type!=2')
			->order("list_order asc,id desc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['gifticon']=get_upload_path($v['gifticon']);
			$v['swf']=get_upload_path($v['swf']);
            return $v;           
        });
        
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	$this->assign("type", $this->getTypes());
    	$this->assign("mark", $this->getMark());
    	$this->assign("swftype", $this->getSwftype());
    	
    	return $this->fetch();
    }
    
	function del(){
        
        $id = $this->request->param('id', 0, 'intval');

        //判断该礼物是不是被设置为了大转盘奖品
        $turntable_gift = Db::name("turntable")->where(['type'=>2,'type_val'=>$id])->find();

        if($turntable_gift){
            $this->error(lang('DELETE_GIFT_FIRST_FROM_WHEEL'));
        }

        //判断该礼物是不是被设置为了星球探宝奖品
        $xqtb_gift = Db::name("xqtb_gift")->where(['type'=>1,'giftid'=>$id])->find();
        
        if($xqtb_gift){
            $this->error(lang('DELETE_GIFT_FIRST_FROM_TREASURE_PLANET'));
        }

        //判断该礼物是不是被设置为了幸运大转盘奖品
        $xydzp_gift = Db::name("xydzp_gift")->where(['type'=>1,'giftid'=>$id])->find();
        
        if($xydzp_gift){
            $this->error(lang('DELETE_GIFT_FIRST_FROM_LUCKY_WHEEL'));
        }


        $rs = DB::name('gift')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }

        //将首充规则里的热门礼物改为0
        Db::name("charge_rules")->where(['giftid'=>$id])->update(['giftid'=>0,'gift_num'=>0]);
        $key='getFirstChargeRules';
        delcache($key);

        $action="删除礼物：{$id}";
        setAdminLog($action);
                    
        $this->resetcache();
        $this->success(lang("DELETE_SUCCESS"));
        
	}
    
    /* 全站飘屏 */
    function plat(){
        
        $id = $this->request->param('id', 0, 'intval');
        $isplatgift = $this->request->param('isplatgift', 0, 'intval');
        
        $rs = DB::name('gift')->where("id={$id}")->update(['isplatgift'=>$isplatgift]);
        if(!$rs){
            $this->error(lang("OPERATION_FAILED"));
        }
        
        $action="修改礼物：{$id}";
        setAdminLog($action);
                    
        $this->resetcache();
        $this->success(lang('OPERATION_SUCCESSFUL'));
	}
    
    //排序
    public function listOrder() {
        $model = DB::name('gift');
        parent::listOrders($model);
        
        $action="更新礼物排序";
        setAdminLog($action);
        
        $this->resetcache();
        $this->success(lang('SORT_UPDATE_SUCCESS'));
    }

    function add(){
        
        $this->assign("type", $this->getTypes());
    	$this->assign("mark", $this->getMark());
    	$this->assign("swftype", $this->getSwftype());
        return $this->fetch();				
    }

	function addPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
            $giftname=$data['giftname'];
            $giftname_en=$data['giftname_en'];

            if($giftname == ''){
                $this->error(lang('PLEASE_ENTER_CHINESE_NAME'));
            }else{
                $check = Db::name('gift')->where("giftname='{$giftname}'")->find();
                if($check){
                    $this->error(lang('NAME_ALREADY_EXISTS_IN_CHINESE'));
                }
            }

            if($giftname_en == ''){
                $this->error(lang("PLEASE_ENTER_ENGLISH_NAME"));
            }else{
                $check = Db::name('gift')->where("giftname_en='{$giftname_en}'")->find();
                if($check){
                    $this->error(lang('NAME_ALREADY_EXISTS_IN_ENGLISH'));
                }
            }

            $needcoin = $data['needcoin'];
            $gifticon = $data['gifticon'];

            if ($needcoin == '') {
                $this->error(lang('PLEASE_ENTER_PRICE'));
            }

            if (!is_numeric($needcoin)) {
                $this->error(lang('PRICE_MUST_BE_NUMERIC'));
            }

            if ($needcoin < 1) {
                $this->error(lang('PRICE_MUST_BE_GREATER_THAN_ONE'));
            }

            if (!$needcoin) {
                $this->error(lang('PRICE_MUST_BE_GREATER_THAN_ONE'));
            }

            if (floor($needcoin) != $needcoin) {
                $this->error(lang('PRICE_MUST_BE_AN_INTEGER_GREATER_THAN_ONE'));
            }

            if ($gifticon == '') {
                $this->error(lang('PLEASE_UPLOAD_IMAGE'));
            }

            $swftype=$data['swftype'];
            $data['swf']=$data['gif'];
            if($swftype==1){
                $data['swf']=$data['svga'];
            }
            
            if($data['type']==1 && $data['swf']==''){
                $this->error(lang('PLEASE_UPLOAD_ANIMATION_EFFECT'));
            }

            $data['gifticon']=set_upload_path($data['gifticon']);
            if($data['gif']){
                $data['gif']=set_upload_path($data['gif']);
            }
            if($data['svga']){
                $data['svga']=set_upload_path($data['svga']);
            }
            
            $data['addtime']=time();
            unset($data['gif']);
            unset($data['svga']);
            
			$id = DB::name('gift')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            
            $action="添加礼物：{$id}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
		}			
	}
    
    function edit(){

        $id  = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('gift')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign("type", $this->getTypes());
    	$this->assign("mark", $this->getMark());
    	$this->assign("swftype", $this->getSwftype());
        $this->assign('data', $data);
        return $this->fetch();            
    }
    
	function editPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();

            $id=$data['id'];
            $giftname=$data['giftname'];
            $giftname_en=$data['giftname_en'];
            if($giftname == ''){
                $this->error(lang('PLEASE_ENTER_CHINESE_NAME'));
            }else{
                $check = Db::name('gift')->where("giftname='{$giftname}' and id!={$id}")->find();
                if($check){
                    $this->error(lang('NAME_ALREADY_EXISTS_IN_CHINESE'));
                }
            }
            
            if($giftname_en == ''){
                $this->error(lang("PLEASE_ENTER_ENGLISH_NAME"));
            }else{
                $check = Db::name('gift')->where("giftname_en='{$giftname_en}' and id!={$id}")->find();
                if($check){
                    $this->error(lang('NAME_ALREADY_EXISTS_IN_ENGLISH'));
                }
            }
            
            $needcoin=$data['needcoin'];
            $gifticon=$data['gifticon'];

            if ($needcoin == '') {
                $this->error(lang('PLEASE_ENTER_PRICE'));
            }

            if (!is_numeric($needcoin)) {
                $this->error(lang('PRICE_MUST_BE_NUMERIC'));
            }

            if ($needcoin < 1) {
                $this->error(lang('PRICE_MUST_BE_INTEGER_GREATER_THAN_1'));
            }

            if (!$needcoin) {
                $this->error(lang('PRICE_MUST_BE_INTEGER_GREATER_THAN_1'));
            }

            if (floor($needcoin) != $needcoin) {
                $this->error(lang('PRICE_MUST_BE_INTEGER_GREATER_THAN_1'));
            }

            if ($gifticon == '') {
                $this->error(lang('PLEASE_UPLOAD_IMAGE'));
            }

            $gifticon_old=$data['gifticon_old'];
            if($gifticon!=$gifticon_old){
                $data['gifticon']=set_upload_path($gifticon);
            }

            $gif=$data['gif'];
            if($gif){
                $gif_old=$data['gif_old'];
                if($gif!=$gif_old){
                    $data['gif']=set_upload_path($gif);
                }
            }

            $svga=$data['svga'];
            if($svga){
                $svga_old=$data['svga_old'];
                if($svga!=$svga_old){
                    $data['svga']=set_upload_path($svga);
                }
            }
            
            $swftype=$data['swftype'];
            $data['swf']=$data['gif'];
            if($swftype==1){
                $data['swf']=$data['svga'];
            }
            if($data['type']==1 && $data['swf']==''){
                $this->error(lang('PLEASE_UPLOAD_ANIMATION_EFFECT'));
            }
            unset($data['gif']);
            unset($data['svga']);
            unset($data['gifticon_old']);
            unset($data['gif_old']);
            unset($data['svga_old']);
            
            if($data['type']!=1){
                $data['isplatgift']=0;
            }
            
			$rs = DB::name('gift')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            $action="修改礼物：{$data['id']}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}	
	}
        
    function resetcache(){
        $key='getGiftList';
        
		$rs=DB::name('gift')
			->field("id,type,mark,giftname,giftname_en,needcoin,gifticon,sticker_id,swftime,isplatgift")
            ->where('type!=2')
			->order("list_order asc,id desc")
			->select();
        if($rs){
            setcaches($key,$rs);
        }else{
			delcache($key);
		}
        return 1;
    }
}

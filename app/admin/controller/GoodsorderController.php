<?php

/**
 * 商品订单列表
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class GoodsorderController extends AdminbaseController {

    protected function getType($k=''){
        $type=[
            '0'=>'',
            '1'=>lang('ALIPAY'),
            '2'=>lang('WECHAT'),
            '3'=>lang('BALANCE'),
            '4'=>lang('WECHAT_MINI_PROGRAM'),
            '5'=>lang('PAYPAL'),
            '6'=>lang('BRAIN_TREE_PAYPAL'),
        ];
        if($k===''){
            return $type;
        }
        return $type[$k] ?? '';
    }

    protected function getRefundStatus($k=''){
        $mark=[
            '-2'=>lang('BUYER_CANCEL_REQUEST'),
            '-1'=>lang('FAILURE'),
            '0'=>lang('PROCESSING'),
            '1'=>lang('SUCCESS'),
        ];
        if($k===''){
            return $mark;
        }
        return $mark[$k] ?? '';
    }
    
    protected function getStatus($k=''){
        $status = [
            '-1' => lang('CLOSED'),
            '0'  => lang('PENDING_PAYMENT'),
            '1'  => lang('PENDING_SHIPMENT'),
            '2'  => lang('PENDING_RECEIPT'),
            '3'  => lang('PENDING_REVIEW'),
            '4'  => lang('REVIEWED'),
            '5'  => lang('REFUND'),
        ];
        if($k===''){
            return $status;
        }
        return $status[$k] ?? '';
    }

    protected function getDelType($k=''){
        $status = [
            '-1' => lang('BUYER_DELETED'),
            '-2' => lang('SELLER_DELETED'),
            '1'  => lang('BOTH_DELETED'),
            '0'  => lang('NOT_DELETED'),
        ];
        if($k===''){
            return $status;
        }
        return $status[$k] ?? '';
    }
    
    function index(){

        $data = $this->request->param();
        $map=[];

        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }


        $orderno= $data['orderno'] ?? '';
        if($orderno!=''){
            $map[]=['orderno','like','%'.$orderno.'%'];
        }

        $trade_no= $data['trade_no'] ?? '';
        if($trade_no!=''){
            $map[]=['trade_no','like','%'.$trade_no.'%'];
        }

        
        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }

        $type= $data['type'] ?? '';
        if($type!=''){
            if($type==0){
                $map[]=['shop_uid','=',1];
            }else{
                $map[]=['shop_uid','>',1]; 
            }
            
        }

        $share_income= $data['share_income'] ?? '';
        if($share_income!=''){
            if($share_income==0){
                $map[]=['share_income','=',0];
            }else{
                $map[]=['share_income','>',0]; 
            }
            
        }

        $buyer_uid=$data['buyer_uid'] ?? '';

        if($buyer_uid!=''){
            $lianguid=getLianguser($buyer_uid);
            if($lianguid){
                
                array_push($lianguid,$buyer_uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$buyer_uid];
            }
        }

        $seller_uid= $data['seller_uid'] ?? '';
        
        if($seller_uid!=''){
            $lianguid=getLianguser($seller_uid);
            if($lianguid){
                
                array_push($lianguid,$seller_uid);
                $map[]=['shop_uid','in',$lianguid];
            }else{
                $map[]=['shop_uid','=',$seller_uid];
            }
        }

        $goods_name= $data['goods_name'] ?? '';
        if($goods_name!=''){
            $map[]=['goods_name','like','%'.$goods_name.'%'];
        }

        $phone= $data['phone'] ?? '';
        if($phone!=''){
            $map[]=['phone','like','%'.$phone.'%'];
        }

    	$lists = Db::name("shop_order")
            ->where($map)
			->order("addtime desc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['spec_thumb']=get_upload_path($v['spec_thumb']);
            $v['buyer_info']=getUserInfo($v['uid']);
            $v['seller_info']=getUserInfo($v['shop_uid']);
            if($v['shareuid']){
                $v['share_info']=getUserInfo($v['shareuid']);
            }
            
            return $v;           
        });
        
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	$this->assign("status", $this->getStatus());
        $this->assign("type", $this->getType());
        $this->assign("refund_status", $this->getRefundStatus());
        $this->assign("del_type", $this->getDelType());
    	
    	return $this->fetch();
    }

    //获取商品订单详情
    public function info(){
        $id=$this->request->param('id');
        $info=Db::name("shop_order")->where("id={$id}")->find();
        if(!$info){
            $this->error(lang('GOODS_ORDER_NOT_EXIST'));
        }

        $info['buyer_info']=getUserInfo($info['uid']);
        $info['seller_info']=getUserInfo($info['shop_uid']);
        $info['spec_thumb']=get_upload_path($info['spec_thumb']);

        $this->assign('data',$info);
        $this->assign("status", $this->getStatus());
        $this->assign("type", $this->getType());
        $this->assign("refund_status", $this->getRefundStatus());
        $this->assign("del_type", $this->getDelType());
        return $this->fetch();

    }

    //填写物流信息
    public function setexpress(){
        $id=$this->request->param("id");
        //获取物流公司列表
        $key='getExpressList';
        $express_list=getcaches($key);

        if(!$express_list){
            $express_list=DB::name('shop_express')
            ->field("id,express_name,express_phone,express_thumb")
            ->where('express_status=1')
            ->order("list_order asc,id desc")
            ->select();
        }
        
        $this->assign("orderid",$id);
        $this->assign("express_list",$express_list);
        return $this->fetch();
    }

    //填写物流单号提交
    public function setexpressPost(){
        $data=$this->request->param();
        $express_id=$data['express_id'];
        $express_number=trim($data['express_number']);
        $orderid=trim($data['orderid']);
        //获取物流信息
        $express_info=Db::name("shop_express")->where("id='{$express_id}'")->find();
        if(!$express_info){
            $this->error(lang('LOGISTICS_COMPANY_NOT_EXIST'));
        }

        if(!$express_number){
            $this->error(lang('PLEASE_FILL_IN_THE_LOGISTICS_NUMBER'));
        }

        if (preg_match("/[\x7f-\xff]/", $express_number)) {
            $this->error(lang('PLEASE_CONFIRM_THE_CORRECT_LOGISTICS_NUMBER'));
        }

        $now=time();

        //语言包
        $data=array(
            'express_name'=>$express_info['express_name'],
            'express_name_en'=>$express_info['express_name_en'],
            'express_phone'=>$express_info['express_phone'],
            'express_thumb'=>$express_info['express_thumb'],
            'express_code'=>$express_info['express_code'],
            'express_number'=>$express_number,
            'status'=>2,
            'shipment_time'=>$now
        );

        $res=Db::name("shop_order")->where("id='{$orderid}'")->update($data);
        if($res===false){
            $this->error(lang('LOGISTICS_INFORMATION_FILL_FAILED'));
        }

        //写入订单消息列表
        $orderinfo=getShopOrderInfo(['id'=>$orderid]);

        $title="你购买的“".$orderinfo['goods_name']."”商家已经发货,物流单号为:".$express_number;
        $title_en="The ".$orderinfo['goods_name']." you purchased has been shipped by the merchant, and the logistics order number is:".$express_number;

        $data1=array(
            'uid'=>$orderinfo['uid'],
            'orderid'=>$orderid,
            'title'=>$title,
            'title_en'=>$title_en,
            'addtime'=>$now,
            'type'=>'0'

        );

        addShopGoodsOrderMessage($data1);

        //发送腾讯IM
        $im_msg=[
            'zh-cn'=>$title,
            'en'=>$title_en,
            'method'=>'order'
        ];
        txMessageIM(json_encode($im_msg),$orderinfo['uid'],'goodsorder_admin','TIMCustomElem');

        $this->success(lang('LOGISTICS_INFO_SUCCESS'), url('admin/Goodsorder/index'));

    }
    

}

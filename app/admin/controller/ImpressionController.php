<?php

/**
 * 印象标签
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class ImpressionController extends AdminbaseController {

    function index(){
        
        $lists = Db::name("label")
			->order("list_order asc,id desc")
			->paginate(20);

        $page = $lists->render();
    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }
    
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('label')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        $action="删除印象标签：{$id}";
        setAdminLog($action);
                    
        $this->resetcache();
        $this->success(lang("DELETE_SUCCESS"));
	}
    
    //排序
    public function listOrder() {
        $model = DB::name('label');
        parent::listOrders($model);
        
        $action="更新印象标签排序";
        setAdminLog($action);
        
        $this->resetcache();
        $this->success(lang('SORT_UPDATE_SUCCESS'));
    }
    

    function add(){        
        return $this->fetch();
    }

    function addPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
			$name=$data['name'];
            $name_en=$data['name_en'];

			if($name==""){
				$this->error(lang('PLEASE_FILL_IN_CHINESE_NAME'));
			}

            if($name_en==""){
                $this->error(lang('PLEASE_FILL_IN_THE_ENGLISH_NAME'));
            }

			$colour=$data['colour'];
			if($colour==""){
				$this->error(lang('PLEASE_SELECT_THE_FIRST_COLOR'));
			}

			$colour2=$data['colour2'];
			if($colour2==""){
				$this->error(lang('PLEASE_SELECT_THE_LAST_COLOR'));
			}
            
			$id = DB::name('label')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            
            $action="添加印象标签：{$id}";
            setAdminLog($action);
            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
		}
	}
    
	function edit(){
        $id = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('label')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign('data', $data);
        return $this->fetch();
	}
    
    function editPost(){
		if ($this->request->isPost()) {
            
            $data  = $this->request->param();
            
			$name=$data['name'];
            $name_en=$data['name_en'];

			if($name==""){
				$this->error(lang('PLEASE_FILL_IN_CHINESE_NAME'));
			}

            if($name_en==""){
                $this->error(lang('PLEASE_FILL_IN_THE_ENGLISH_NAME'));
            }

			$colour=$data['colour'];
			if($colour==""){
                $this->error(lang('PLEASE_SELECT_THE_FIRST_COLOR'));
			}

			$colour2=$data['colour2'];
			if($colour2==""){
                $this->error(lang('PLEASE_SELECT_THE_LAST_COLOR'));
			}
            
			$rs = DB::name('label')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            $action="修改印象标签：{$data['id']}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}
	}
    
    
    function resetCache(){
        $key='getImpressionLabel';
        $rules= DB::name("label")
            ->order('list_order asc,id desc')
            ->select();
        if($rules){
            setcaches($key,$rules);
        }else{
			delcache($key);
		}
        
        return 1;
    }
}

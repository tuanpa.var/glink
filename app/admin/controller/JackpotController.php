<?php

/**
 * 奖池设置
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class JackpotController extends AdminbaseController {

    function set(){

        $config=Db::name("option")->where("option_name='jackpot'")->value("option_value");

		$this->assign('config',json_decode($config,true) );
    	
    	return $this->fetch();
    }
    
    function setPost(){
        if ($this->request->isPost()) {
            
            $config = $this->request->param('post/a');

            $luck_anchor=$config['luck_anchor'];
            $luck_jackpot=$config['luck_jackpot'];

            if($luck_anchor==''){
                $this->error(lang('LUCK_ANCHOR_IS_NOT_SET'));
            }

            if(!is_numeric($luck_anchor)){
                $this->error(lang('ANCHOR_RATIO_MUST_BE_NUMERIC'));
            }

            if($luck_anchor<0||$luck_anchor>100){
                $this->error(lang('ANCHOR_RATIO_MUST_BE_BETWEEN_0_100'));
            }

            if(floor($luck_anchor)!=$luck_anchor){
                $this->error(lang('ANCHOR_RATIO_MUST_BE_INTEGER'));
            }

            if($luck_jackpot==''){
                $this->error(lang('PLEASE_FILL_IN_THE_PRIZE_POOL_RATIO'));
            }

            if(!is_numeric($luck_jackpot)){
                $this->error(lang('PRIZE_POOL_RATIO_MUST_BE_NUMERIC'));
            }

            if($luck_jackpot<0||$luck_jackpot>100){
                $this->error(lang('PRIZE_POOL_RATIO_MUST_BE_BETWEEN_0_100'));
            }

            if(floor($luck_jackpot)!=$luck_jackpot){
                $this->error(lang('PRIZE_POOL_RATIO_MUST_BE_AN_INTEGER'));
				
            }

			//查询已存在的内容
			$info=DB::name("option")->where("option_name='jackpot'")->value("option_value");

            if(!$info){
                $rs = DB::name("option")->insertGetId(['option_name'=>'jackpot','option_value'=>json_encode($config)]);
                $info=json_encode($config);
            }else{
                $rs = DB::name('option')->where("option_name='jackpot'")->update(['option_value'=>json_encode($config)] );
            }

            if($rs===false){
                $this->error(lang('EDIT_FAILED'));
            }
            $key='jackpotset';
            setcaches($key,$config);

			if($info){
				$option_value=json_decode($info,true);
				
				$action="修改奖池管理 ";
				
				if($config['switch'] !=$option_value['switch']){
					$switch=$config['switch']?'开':'关';
					$action.='奖池开关 '.$switch.' ';
				}

				if($config['luck_anchor'] !=$option_value['luck_anchor']){
					$action.='幸运礼物-主播比例 '.$config['luck_anchor'].' ';
				}
				
				if($config['luck_jackpot'] !=$option_value['luck_jackpot']){
					$action.='幸运礼物-奖池比例 '.$config['luck_jackpot'].' ';
				}
				
				setAdminLog($action);
			}

            $this->success(lang('EDIT_SUCCESS'));
		}
    }

    /**
     * @desc 奖池等级
     * @return mixed
     * @throws \think\db\exception\DbException
     */
    function index(){
        
        $lists = Db::name("jackpot_level")
			->order("levelid asc")
			->paginate(20);

        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }

    /**
     * @desc 奖池等级删除
     * @return void
     * @throws \think\db\exception\DbException
     */
	function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('jackpot_level')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
		$action="删除奖池管理-列表ID: ".$id;
		setAdminLog($action);
                    
        $this->resetcache();
        $this->success(lang("DELETE_SUCCESS"));
            
	}

    /**
     * @desc 奖池等级添加
     * @return mixed
     */
    function add(){
		return $this->fetch();
	}

    /**
     * @desc 奖池等级添加保存
     * @return void
     */
    function addPost(){
		if ($this->request->isPost()) {
            
            $data   = $this->request->param();
            
			$levelid=$data['levelid'];

			if($levelid==""){
				$this->error(lang('PLEASE_FILL_IN_THE_LEVEL'));
			}
            
            $check = DB::name('jackpot_level')->where(["levelid"=>$levelid])->find();
            if($check){
                $this->error(lang('LEVEL_CANNOT_REPEAT'));
            }
                
			$level_up=$data['level_up'];
			if($level_up==""){
				$this->error(lang('PLEASE_FILL_IN_THE_LOWER_LIMIT_OF_LEVEL'));
			}
            
            $data['addtime']=time();
            
			$id = DB::name('jackpot_level')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            $action="添加奖池管理-列表等级: ".$id;
			setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
		}
	}

    /**
     * @desc 奖池等级修改
     * @return mixed
     */
    function edit(){
        
        $id  = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('jackpot_level')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign('data', $data);
        return $this->fetch();
	}

    /**
     * @desc 奖池等级编辑保存
     * @return void
     */
	function editPost(){
		if ($this->request->isPost()) {
            
            $data  = $this->request->param();
            
			$id=$data['id'];
			$levelid=$data['levelid'];

			if($levelid==""){
				$this->error(lang('PLEASE_FILL_IN_THE_LEVEL'));
			}
            
            $check = DB::name('jackpot_level')->where([["levelid",'=',$levelid],['id','<>',$id]])->find();
            if($check){
                $this->error(lang('LEVEL_CANNOT_REPEAT'));
            }
                
			$level_up=$data['level_up'];
			if($level_up==""){
				$this->error(lang('PLEASE_FILL_IN_THE_LOWER_LIMIT_OF_LEVEL'));
			}

            if($level_up<0){
                $this->error(lang('THE_LOWER_LIMIT_OF_LEVEL_CANNOT_BE_LESS_THAN_ZERO'));
            }

            if(!is_numeric($level_up)){
                $this->error(lang('THE_LOWER_LIMIT_OF_LEVEL_MUST_BE_A_NUMBER'));
            }

            if(floor($level_up)!=$level_up){
                $this->error(lang('THE_LOWER_LIMIT_OF_LEVEL_MUST_BE_AN_INTEGER'));
            }
            
			$rs = DB::name('jackpot_level')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            $action="修改奖池管理-列表等级：{$data['id']}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}
	}
    
    function resetcache(){
        $key='jackpot_level';

        $level= DB::name("jackpot_level")->order("level_up asc")->select();
        if($level){
            setcaches($key,$level);
        }else{
			delcache($key);
		}
        
        return 1;
    }       

}

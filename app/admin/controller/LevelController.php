<?php

/**
 * 经验等级
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class LevelController extends AdminbaseController {
	
    function index(){
        
    	$lists = Db::name("level")
			->order("levelid asc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['thumb']=get_upload_path($v['thumb']);
			$v['thumb_mark']=get_upload_path($v['thumb_mark']);
			$v['bg']=get_upload_path($v['bg']);
            return $v;           
        });
        
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }
    
    function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('level')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        $action="删除会员等级：{$id}";
        setAdminLog($action);
                    
        $this->resetcache();
        $this->success(lang('DELETE_SUCCESS'),url("level/index"));
            
	}		

	function add(){
		return $this->fetch();
	}
    
	function addPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$levelid=$data['levelid'];
            $levelname=$data['levelname'];
            $levelname_en=$data['levelname_en'];

			if($levelid==""){
				$this->error(lang('LEVEL_CANNOT_BE_EMPTY'));
			}

            if(!is_numeric($levelid)){
                $this->error(lang('LEVEL_MUST_BE_NUMERIC'));
            }

            if($levelid<1){
                $this->error(lang('LEVEL_MUST_BE_GREATER_THAN_ONE'));
            }

            if($levelid>99999999){
                $this->error(lang('THE_LEVEL_MUST_BE_BETWEEN_1_AND_99999999'));
            }

            if(floor($levelid)!=$levelid){
                $this->error(lang('LEVEL_MUST_BE_AN_INTEGER'));
            }
            
            $check = Db::name('level')->where(["levelid"=>$levelid])->find();
            if($check){
                $this->error(lang('LEVEL_CANNOT_REPEAT'));
            }

            if($levelname==''){
                $this->error(lang('PLEASE_FILL_IN_THE_CHINESE_NAME_OF_THE_LEVEL'));
            }

            if($levelname_en==''){
                $this->error(lang('PLEASE_FILL_IN_THE_ENGLISH_NAME_OF_THE_LEVEL'));
            }
                
			$level_up=$data['level_up'];
			if($level_up==""){
				$this->error(lang('PLEASE_FILL_IN_THE_UPPER_LIMIT_OF_EXPERIENCE_LEVEL'));
			}

            if(!is_numeric($level_up)){
                $this->error(lang('THE_UPPER_LIMIT_OF_EXPERIENCE_LEVEL_MUST_BE_A_NUMBER'));
            }

            if($level_up<1||$level_up>9999999999){
                $this->error(lang('THE_EXPERIENCE_LIMIT_OF_THE_LEVEL_MUST_BE_A_NUMBER_BETWEEN_1_AND_9999999999'));
            }

            if(floor($level_up)!=$level_up){
                $this->error(lang('THE_EXPERIENCE_LIMIT_OF_THE_LEVEL_MUST_BE_AN_INTEGER'));
            }

			$colour=$data['colour'];
			if($colour==""){
				$this->error(lang('PLEASE_FILL_IN_NICKNAME_COLOR'));
			}
            
            $thumb=$data['thumb'];
			if($thumb==""){
				$this->error(lang('PLEASE_UPLOAD_ICON'));
			}

            $data['thumb']=set_upload_path($thumb);
            
            $thumb_mark=$data['thumb_mark'];
			if($thumb_mark==""){
				$this->error(lang('PLEASE_UPLOAD_THE_AVATAR_BADGE'));
			}

            $data['thumb_mark']=set_upload_path($thumb_mark);

            $bg=$data['bg'];
            if($bg==""){
                $this->error(lang('PLEASE_UPLOAD_THE_BACKGROUND_IMAGE'));
            }

            $data['bg']=set_upload_path($bg);
            
            $data['addtime']=time();
            
			$id = DB::name('level')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            
            $action="添加会员等级：{$id}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
            
		}			
	}
        
	function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('level')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign('data', $data);
        return $this->fetch();
	}
    
    function editPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$id=$data['id'];
			$levelid=$data['levelid'];
            $levelname=$data['levelname'];
            $levelname_en=$data['levelname_en'];

			if($levelid==""){
				$this->error(lang('LEVEL_CANNOT_BE_EMPTY'));
			}

            if($levelid==""){
                $this->error(lang('LEVEL_CANNOT_BE_EMPTY'));
            }

            if(!is_numeric($levelid)){
                $this->error(lang('LEVEL_MUST_BE_NUMERIC'));
            }

            if($levelid<1){
                $this->error(lang('LEVEL_MUST_BE_GREATER_THAN_ONE'));
            }

            if($levelid>99999999){
                $this->error(lang('THE_LEVEL_MUST_BE_BETWEEN_1_AND_99999999'));
            }

            if(floor($levelid)!=$levelid){
                $this->error(lang('LEVEL_MUST_BE_AN_INTEGER'));
            }
            
            $check = Db::name('level')->where([['levelid','=',$levelid],['id','<>',$id]])->find();
            if($check){
                $this->error(lang('LEVEL_CANNOT_REPEAT'));
            }

            if($levelname==''){
                $this->error(lang('PLEASE_FILL_IN_THE_CHINESE_NAME_OF_THE_LEVEL'));
            }

            if($levelname_en==''){
                $this->error(lang('PLEASE_FILL_IN_THE_ENGLISH_NAME_OF_THE_LEVEL'));
            }
                
			$level_up=$data['level_up'];
			if($level_up==""){
				$this->error(lang('PLEASE_FILL_IN_THE_UPPER_LIMIT_OF_EXPERIENCE_LEVEL'));
			}

            if(!is_numeric($level_up)){
                $this->error(lang('THE_UPPER_LIMIT_OF_EXPERIENCE_LEVEL_MUST_BE_A_NUMBER'));
            }

            if($level_up<1||$level_up>9999999999){
                $this->error(lang('THE_EXPERIENCE_LIMIT_OF_THE_LEVEL_MUST_BE_A_NUMBER_BETWEEN_1_AND_9999999999'));
            }

            if(floor($level_up)!=$level_up){
                $this->error(lang('THE_EXPERIENCE_LIMIT_OF_THE_LEVEL_MUST_BE_AN_INTEGER'));
            }

			$colour=$data['colour'];
			if($colour==""){
				$this->error(lang('PLEASE_FILL_IN_NICKNAME_COLOR'));
			}
            
            $thumb=$data['thumb'];
			if($thumb==""){
				$this->error(lang('PLEASE_UPLOAD_ICON'));
			}

            $thumb_old=$data['thumb_old'];
            if($thumb!=$thumb_old){
                $data['thumb']=set_upload_path($thumb);
            }
            
            $thumb_mark=$data['thumb_mark'];
			if($thumb_mark==""){
				$this->error(lang('PLEASE_UPLOAD_THE_AVATAR_BADGE'));
			}

            $thumb_mark_old=$data['thumb_mark_old'];
            if($thumb_mark!=$thumb_mark_old){
                $data['thumb_mark']=set_upload_path($thumb_mark);
            }

            $bg=$data['bg'];
            if($bg==""){
                $this->error(lang('PLEASE_UPLOAD_THE_BACKGROUND_IMAGE'));
            }

            $bg_old=$data['bg_old'];
            if($bg_old!=$bg){
                $data['bg']=set_upload_path($bg);
            }

            unset($data['thumb_old']);
            unset($data['thumb_mark_old']);
            unset($data['bg_old']);

			$rs = DB::name('level')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            $action="修改会员等级：{$data['id']}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}
	}
    
    function resetcache(){
		$key='level';
        
        $level= Db::name("level")->order("level_up asc")->select();
        if($level){
            setcaches($key,$level);
        }else{
			delcache($key);
		}
        
        return 1;
    }
		
}

<?php

/**
 * 靓号管理
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class LiangController extends AdminbaseController {
    
    protected function getStatus($k=''){
        $status=[
            '0'=>lang('SALE'),
            '1'=>lang('SOLD'),
            '2'=>lang('STOP_SALE'),
        ];
        
        if($k===''){
            return $status;
        }
        
        return $status[$k];
    }

    function index(){
        
        $data = $this->request->param();
        $map=[];
        
        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }
        
        $length= $data['length'] ?? '';
        if($length!=''){
            $map[]=['length','=',$length];
        }
        
        $uid= $data['uid'] ?? '';
        if($uid!=''){
            $map[]=['uid','=',$uid];
        }
        
        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['name','like','%'.$keyword.'%'];
        }
			

    	$lists = Db::name("liang")
                ->where($map)
                ->order("id DESC")
                ->paginate(20);
                
        $lists->each(function($v,$k){
			if($v['uid']>0){
				$v['userinfo']=getUserInfo($v['uid']);
			}
            return $v;           
        });
                
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
        $this->assign('status', $this->getStatus());
        
        $length=Db::name("liang")
			->field("length")
			->order("length asc")
			->group("length")
			->select();

    	$this->assign('length', $length);

    	return $this->fetch();
    }

	function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $info = DB::name('liang')->where("id={$id}")->find();
        $rs = DB::name('liang')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        if($info['uid']>0){
            $key='liang_'.$info['uid'];
            delcache($key);
        }
                    
        $action="删除靓号：{$id}";
        setAdminLog($action);
        
        $this->success(lang('DELETE_SUCCESS'),url("liang/index"));
	}

	function setStatus(){
        
        $id = $this->request->param('id', 0, 'intval');
        $status = $this->request->param('status', 0, 'intval');

        $result=DB::name('liang')->where(["id"=>$id])->update(['status'=>$status]);
        if($result!==false){
            if($status==1){
                $action="修改靓号状态：{$id} - " . lang('SOLD');
            }else if($status==2){
                $action="修改靓号状态：{$id} - " . lang('STOP_SALE');
            }else{
                $action="修改靓号状态：{$id} - 出售中";
            }

            setAdminLog($action);
            $this->success(lang('OPERATION_SUCCESS'));
        }
            
        $this->error(lang('OPERATION_FAILED'));		
	}

    //排序
    public function listOrder() { 
		
        $model = DB::name('liang');
        parent::listOrders($model);
        
        $action="修改靓号排序";
        setAdminLog($action);
        
        $this->success(lang('SORT_UPDATE_SUCCESS'));
    }

	function add(){
        return $this->fetch();			
	}
    
	function addPost(){
		if ($this->request->isPost()) {
            
            $configpub=getConfigPub();
            
            $data = $this->request->param();
            
			$name=$data['name'];

			if($name==""){
				$this->error(lang('NICE_NUMBER_CANNOT_BE_EMPTY'));
			}
            
			$coin=$data['coin'];
			if($coin==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED').$configpub['name_coin']);
			}

			if(!is_numeric($coin)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED').$configpub['name_coin']);
			}

            if($coin<1||$coin>99999999){
                $this->error(lang('REQUIRED').$configpub['name_coin']."必须在1-99999999之间");
            }
            
            $score=$data['score'];
			if($score==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED').$configpub['name_score']);
			}

			if(!is_numeric($score)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED').$configpub['name_score']);
			}

            if($score<1||$score>99999999){
                $this->error(lang('REQUIRED').$configpub['name_score']."必须在1-99999999之间");
            }
            
            $isexist=DB::name('liang')->where(["name"=>$name])->find();
			
			if($isexist){
				$this->error(lang('THE_PRETTY_ID_ALREADY_EXISTS'));
			}

            $data['length']=mb_strlen($name);
            $data['addtime']=time();
            
			$id = DB::name('liang')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            
            $action="添加靓号：{$id}";
            setAdminLog($action);
            
            $this->success(lang('ADD_SUCCESS'));
		}			
	}    
	
	function edit(){
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('liang')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign('data', $data);
        return $this->fetch();			
	}
	
	function editPost(){
		if ($this->request->isPost()) {
            
            $configpub=getConfigPub();
            
            $data = $this->request->param();
            
			$id=$data['id'];
			$name=$data['name'];

			if($name==""){
				$this->error(lang('NICE_NUMBER_CANNOT_BE_EMPTY'));
			}
            
            $coin=$data['coin'];
			if($coin==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED').$configpub['name_coin']);
			}

			if(!is_numeric($coin)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED').$configpub['name_coin']);
			}

            if($coin<1||$coin>99999999){
                $this->error(lang('REQUIRED').$configpub['name_coin']."必须在1-99999999之间");
            }
            
            $score=$data['score'];
			if($score==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED').$configpub['name_score']);
			}

			if(!is_numeric($score)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED').$configpub['name_score']);
			}
            
            if($score<1||$score>99999999){
                $this->error(lang('REQUIRED').$configpub['name_score']."必须在1-99999999之间");
            }
            
            $isexist=DB::name('liang')->where([['id','<>',$id],['name','=',$name]])->find();
			
			if($isexist){
				$this->error(lang('THE_PRETTY_ID_ALREADY_EXISTS'));
			}
            
			$rs = DB::name('liang')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            $action="编辑靓号：{$data['id']}";
            setAdminLog($action);
            
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}
	}
		
}

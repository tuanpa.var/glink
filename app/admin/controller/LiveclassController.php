<?php

/**
 * 直播分类
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class LiveclassController extends AdminbaseController {
    function index(){
			
    	$lists = Db::name("live_class")
            ->order("list_order asc, id desc")
            ->paginate(20);
        $page = $lists->render();
    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }

    /**
     * @desc 删除
     * @return void
     * @throws \think\db\exception\DbException
     */
    function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('live_class')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }

        Db::name("live")->where(['liveclassid'=>$id])->update(['liveclassid'=>0]);
        Db::name("live_record")->where(['liveclassid'=>$id])->update(['liveclassid'=>0]);
        
        $action="删除直播分类：{$id}";
        setAdminLog($action);
                    
        $this->resetcache();
        $this->success(lang("DELETE_SUCCESS"));				
    }		
    //排序
    public function listOrder() { 
		
        $model = DB::name('live_class');
        parent::listOrders($model);
        
        $action="更新直播分类排序";
        setAdminLog($action);
        
        $this->resetcache();
        $this->success(lang('SORT_UPDATE_SUCCESS'));
    }

    /**
     * @desc 添加
     * @return mixed
     */
    function add(){        
        return $this->fetch();
    }

    /**
     * @desc 添加保存
     * @return void
     */
    function addPost(){
        if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$name=$data['name'];
            $name_en=$data['name_en'];

			if($name==""){
				$this->error(lang('PLEASE_FILL_IN_CHINESE_NAME'));
			}

            if($name_en==""){
                $this->error(lang('PLEASE_FILL_IN_THE_ENGLISH_NAME'));
            }

			$thumb=$data['thumb'];
			if($thumb==""){
				$this->error(lang('PLEASE_UPLOAD_ICON'));
			}

            $data['thumb']=set_upload_path($thumb);

            $des=$data['des'];
            $des_en=$data['des_en'];
            if($des==''){
                $this->error(lang('PLEASE_FILL_IN_LIVE_CATEGORY_DESCRIPTION_IN_CHINESE'));
            }

            if(mb_strlen($des)>200){
                $this->error(lang('LIVE_CATEGORY_DESCRIPTION_IN_CHINESE_SHOULD_BE_LESS_THAN_200_CHARACTERS'));
            }

            if($des_en==''){
                $this->error(lang('PLEASE_FILL_IN_THE_DESCRIPTION_IN_ENGLISH_FOR_THE_LIVE_CATEGORY'));
            }

            if(mb_strlen($des_en)>200){
                $this->error(lang('THE_ENGLISH_DESCRIPTION_FOR_THE_LIVE_CATEGORY_SHOULD_BE_WITHIN_200_CHARACTERS'));
            }
            
			$id = DB::name('live_class')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            
            $action="添加直播分类：{$id}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
		}
    }

    /**
     * @desc 编辑直播分类
     * @return mixed
     */
    function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('live_class')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign('data', $data);
        return $this->fetch(); 			
    }
    
    function editPost(){
        if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
			$name=$data['name'];
            $name_en=$data['name_en'];

			if($name==""){
				$this->error(lang('PLEASE_FILL_IN_CHINESE_NAME'));
			}

            if($name_en==""){
                $this->error(lang('PLEASE_FILL_IN_THE_ENGLISH_NAME'));
            }

			$thumb=$data['thumb'];
			if($thumb==""){
				$this->error(lang('PLEASE_UPLOAD_ICON'));
			}

			$des=$data['des'];
            $des_en=$data['des_en'];
            if($des==''){
                $this->error(lang('PLEASE_FILL_IN_LIVE_CATEGORY_DESCRIPTION_IN_CHINESE'));
            }

            if(mb_strlen($des)>200){
                $this->error(lang('LIVE_CATEGORY_DESCRIPTION_IN_CHINESE_SHOULD_BE_LESS_THAN_200_CHARACTERS'));
            }

            if($des_en==''){
                $this->error(lang('PLEASE_FILL_IN_THE_DESCRIPTION_IN_ENGLISH_FOR_THE_LIVE_CATEGORY'));
            }

            if(mb_strlen($des_en)>200){
                $this->error(lang('THE_ENGLISH_DESCRIPTION_FOR_THE_LIVE_CATEGORY_SHOULD_BE_WITHIN_200_CHARACTERS'));
            }

            $thumb_old=$data['thumb_old'];
            if($thumb_old!=$thumb){
                $data['thumb']=set_upload_path($thumb);
            }

            unset($data['thumb_old']);
            
			$id = DB::name('live_class')->update($data);
            if($id===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            $action="修改直播分类：{$data['id']}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}	
    }
    
    function resetcache(){
        $key='getLiveClass';
        $rules= DB::name('live_class')
                ->order('list_order asc,id desc')
                ->select();
        if($rules){
			
			
            setcaches($key,$rules);
        }else{
			delcache($key);
		}
        
        return 1;
    }
}

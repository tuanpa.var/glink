<?php

/**
 * 管理员手动充值记录
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class ManualController extends AdminbaseController {

    function index(){
        $data = $this->request->param();
        $map=[];
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';

        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }

        $uid=$data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['touid','in',$lianguid];
            }else{
                $map[]=['touid','=',$uid];
            }
        }

        $lists = Db::name("charge_admin")
            ->where($map)
			->order("id desc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['touid']);
			$v['ip']=long2ip($v['ip']);
            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	
        $coin = Db::name("charge_admin")
            ->where($map)
			->sum('coin');
        if(!$coin){
            $coin=0;
        }

    	$this->assign('coin', $coin);
        
    	return $this->fetch();
    }
		
	function add(){
		return $this->fetch();
	}
	function addPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$touid=$data['touid'];

			if($touid==""){
                $this->error(lang('PLEASE_FILL_IN_USER_ID'));
			}
            
            $uid=Db::name("user")->where(["id"=>$touid])->value("id");
            if(!$uid){
                $this->error(lang('MEMBER_DOES_NOT_EXIST'));
                
            }
            
			$coin=$data['coin'];
			if($coin==""){
                $this->error(lang('PLEASE_FILL_IN_RECHARGE_POINTS'));
			}

            if(!is_numeric($coin)){
                $this->error(lang('RECHARGE_POINTS_MUST_BE_NUMERIC'));
            }

            if(floor($coin)!=$coin){
                $this->error(lang('RECHARGE_POINTS_MUST_BE_INTEGER'));
            }

            $user_coin=Db::name("user")->where(["id"=>$touid])->value("coin");

            $total=$user_coin+$coin;
            if($total<0){
                $total=0;
            }
            
            $adminid=cmf_get_current_admin_id();
            $admininfo=Db::name("user")->where(["id"=>$adminid])->value("user_login");
            
            $data['admin']=$admininfo;
            $ip=get_client_ip(0,true);
            
            $data['ip']=ip2long($ip);
            
            $data['addtime']=time();
            
			$id = DB::name('charge_admin')->insertGetId($data);
            if(!$id){
                $this->error(lang('RECHARGE_FAILED'));
            }
			
			$action="手动充值虚拟币ID：".$id;
			setAdminLog($action);
            
            Db::name("user")->where(["id"=>$touid])->update(['coin'=>$total]);
            $this->success(lang('RECHARGE_SUCCESS'));
            
		}
	}
    
    function export(){
        $data = $this->request->param();
        $map=[];
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        $status=$data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }

        $uid=$data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['touid','in',$lianguid];
            }else{
                $map[]=['touid','=',$uid];
            }
        }
        
        $xlsName  = "手动充值记录";
        $xlsData = Db::name("charge_admin")
            ->where($map)
			->order("id desc")
			->select()
            ->toArray();

        if(empty($xlsData)){
            $this->error(lang("DATA_EMPTY"));
        }

        foreach ($xlsData as $k => $v){

            $userinfo=getUserInfo($v['touid']);

            $xlsData[$k]['ip']=long2ip($v['ip']);
            $xlsData[$k]['user_nickname']= $userinfo['user_nickname'].'('.$v['touid'].')';
            $xlsData[$k]['addtime']=date("Y-m-d H:i:s",$v['addtime']); 
        }
        
        $action="导出手动充值记录：".Db::name("charge_admin")->getLastSql();
        setAdminLog($action);
        $cellName = array('A','B','C','D','E','F');
        $xlsCell  = array(
            array('id', lang('SERIAL_NUMBER')),
            array('admin', lang('ADMINISTRATOR')),
            array('user_nickname', lang('MEMBER_ACCOUNT_NUMBER_ID')),
            array('coin', lang('RECHARGE_POINTS')),
            array('ip','IP'),
            array('addtime', lang('TIME')),
        );
        exportExcel($xlsName,$xlsCell,$xlsData,$cellName);
    }

}

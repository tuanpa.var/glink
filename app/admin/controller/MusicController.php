<?php

/**
 * 音乐管理
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use cmf\lib\Upload;


class MusicController extends AdminbaseController {
    
    protected function getTypes($k=''){
        $type=array(
            '1'=>'管理员',
            '2'=>'用户',
        );
        if($k===''){
            return $type;
        }
        return $type[$k] ?? '';
    }
    
    protected function getDel($k=''){
        $type=array(
            '0'=>lang('NO'),
            '1'=>lang('YES'),
        );
        if($k===''){
            return $type;
        }
        return $type[$k] ?? '';
    }
    
    protected function getCat($k=''){
        $list=Db::name("music_classify")
                ->order("list_order asc")
                ->column('title','id');
        
        if($k==''){
            return $list;
        }
        return $list[$k] ?? '';
    }
	
    function index(){
        $data = $this->request->param();
        $map=[];
        
        $classify_id= $data['classify_id'] ?? '';
        if($classify_id!=''){
            $map[]=['classify_id','=',$classify_id];
        }
        
        $upload_type= $data['upload_type'] ?? '';
        if($upload_type!=''){
            $map[]=['upload_type','=',$upload_type];
        }
        
        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['title','like','%'.$keyword.'%'];
        }
			

    	$lists = Db::name("music")
                ->where($map)
                ->order("id DESC")
                ->paginate(20);
        
        $lists->each(function($v,$k){
			$v['img_url']=get_upload_path($v['img_url']);
			$v['file_url']=get_upload_path($v['file_url']);
			$v['uploader_nicename']=Db::name("user")->where(["id"=>$v['uploader']])->value("user_nickname");
            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
        $this->assign('classify', $this->getCat());
        $this->assign('type', $this->getTypes());
        $this->assign('isdel', $this->getDel());
    	
    	return $this->fetch();
    }
    
    /*音乐试听*/
    function listen(){
        $id = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('music')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $data['file_url']=get_upload_path($data['file_url']);
        
        $this->assign('data', $data);
        return $this->fetch();
    }
    
	function del(){
        
        $id = $this->request->param('id', 0, 'intval');

        $count=DB::name("video")->where(["music_id"=>$id])->count();
        if($count>0){
            $rs=DB::name("music")->where(["id"=>$id])->update(array("isdel"=>1));
        }else{
            $rs=DB::name("music")->where(["id"=>$id])->delete();
        }	
            
        if($rs===false){
            $this->error(lang("DELETE_FAILED"));
        }
		
		$action="视频管理-删除音乐ID: ".$id;
		setAdminLog($action);
        
        $this->success(lang('DELETE_SUCCESS'),url("music/index"));
	}
    
    /* 取消删除 */
    function canceldel(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('music')->where("id={$id}")->update(['isdel'=>0]);
        if(!$rs){
            $this->error(lang('CANCEL_FAILED'));
        }
		
		$action="视频管理-取消删除音乐ID: ".$id;
		setAdminLog($action);
        
        $this->success(lang('CANCEL_SUCCESS'), url('music/index'));
	}
    
    /*背景音乐添加*/
    function add(){
    	$this->assign('classify', $this->getCat());
    	return $this->fetch();
    }
    
    function addPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
			$title=$data['title'];

			if($title==""){
				$this->error(lang('PLEASE_FILL_IN_THE_MUSIC_NAME'));
			}
            
            $isexist=DB::name('music')->where(["title"=>$title])->find();
			if($isexist){
				$this->error(lang('THE_MUSIC_ALREADY_EXISTS'));
			}
            
			$author=$data['author'];
			if($author==""){
				$this->error(lang('PLEASE_FILL_IN_THE_SINGER'));
			}

			$img_url=$data['img_url'];
			if($img_url==""){
				$this->error(lang('PLEASE_UPLOAD_MUSIC_COVER'));
			}

            $data['img_url']=set_upload_path($img_url);
            
            $file= $_FILES['file'] ?? '';
            if(!$file){
                $this->error(lang('PLEASE_UPLOAD_MUSIC'));
            }
            
            $res=$this->upload($file);
            if($res['ret']==0){
                $this->error($res['msg']);
            }

            $data['file_url']=set_upload_path($res['data']['url']);

			/* $length=$data['length'];
			if($length==""){
				$this->error(lang('PLEASE_FILL_IN_THE_MUSIC_DURATION'));
			}
            
            if(!strpos($length,":")){
				$this->error(lang('PLEASE_FILL_IN_THE_MUSIC_DURATION_IN_CORRECT_FORMAT'));
			}
            
            $length=$data['length'];
			if($length==""){
				$this->error(lang('PLEASE_FILL_IN_THE_MUSIC_DURATION'));
			} */
            
            $use_nums=$data['use_nums'];
            if(!is_numeric($use_nums)){
                $this->error(lang('USED_TIMES_MUST_BE_A_NUMBER'));
            }

            if($use_nums<0){
                $this->error(lang('USED_TIMES_CANNOT_BE_LESS_THAN_ZERO'));;
            }

            if(floor($use_nums)!=$use_nums){
                $this->error(lang('USED_TIMES_MUST_BE_INTEGER'));
            }
            
            $data['uploader']=cmf_get_current_admin_id();
            $data['upload_type']=1;
            $data['addtime']=time();
            
			$id = DB::name('music')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }

			$action="视频管理-添加音乐ID: ".$id;
			setAdminLog($action);

            $this->success(lang('ADD_SUCCESS'));
		}
	}
    
    /*音乐编辑*/
    function edit(){        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('music')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign('data', $data);
        
        $this->assign('classify', $this->getCat());
        
        return $this->fetch();
    }


    function editPost(){
        
        if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$id=$data['id'];
			$title=$data['title'];

			if($title==""){
				$this->error(lang('PLEASE_FILL_IN_THE_MUSIC_NAME'));
			}
            
            $isexist=DB::name('music')->where([['title','=',$title],['id','<>',$id]])->find();
			if($isexist){
				$this->error(lang('THE_MUSIC_ALREADY_EXISTS'));
			}
            
			$author=$data['author'];
			if($author==""){
				$this->error(lang('PLEASE_FILL_IN_THE_SINGER'));
			}

			$img_url=$data['img_url'];
			if($img_url==""){
				$this->error(lang('PLEASE_UPLOAD_MUSIC_COVER'));
			}

            $img_url_old=$data['img_url_old'];
            if($img_url!=$img_url_old){
                $data['img_url']=set_upload_path($img_url);
            }
            
            $file= $_FILES['file'] ?? '';

            if($file){
                $res=$this->upload($file);
                if($res['ret']==0){
                    $this->error($res['msg']);
                }

                $data['file_url']=set_upload_path($res['data']['url']);
            }
            
            

			/* $length=$data['length'];
			if($length==""){
				$this->error(lang('PLEASE_FILL_IN_THE_MUSIC_DURATION'));
			}
            
            if(!strpos($length,":")){
				$this->error(lang('PLEASE_FILL_IN_THE_MUSIC_DURATION_IN_CORRECT_FORMAT'));
			}
            
            $length=$data['length'];
			if($length==""){
				$this->error(lang('PLEASE_FILL_IN_THE_MUSIC_DURATION'));
			} */

            $use_nums=$data['use_nums'];
            if(!is_numeric($use_nums)){
                $this->error(lang('USED_TIMES_MUST_BE_A_NUMBER'));
            }

            if($use_nums<0){
                $this->error(lang('USED_TIMES_CANNOT_BE_LESS_THAN_ZERO'));;
            }

            if(floor($use_nums)!=$use_nums){
                $this->error(lang('USED_TIMES_MUST_BE_INTEGER'));
            }
            
            $data['updatetime']=time();
            unset($data['file']);
            unset($data['img_url_old']);
			$rs = DB::name('music')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }

			$action="视频管理-修改音乐ID: ".$id;
			setAdminLog($action);

            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}

    }
    
    protected function upload($file){
        
        $configpri=getConfigPri();
        $cloudtype=$configpri['cloudtype'];
        if($cloudtype==1){
            $uploader = new Upload();
            $uploader->setFileType('audio');
            $result = $uploader->upload();

        }else{
            $name=adminUploadFiles($file,2);
            if(!$name){
                $result=false;
            }else{
                $result['filepath']=$name;
            }
        }

        if ($result === false) {
            return array("ret"=>0,'file'=>'','msg'=>$uploader->getError());
        }
        /* $result=[
            'filepath'    => $arrInfo["file_path"],
            "name"        => $arrInfo["filename"],
            'id'          => $strId,
            'preview_url' => cmf_get_root() . '/upload/' . $arrInfo["file_path"],
            'url'         => cmf_get_root() . '/upload/' . $arrInfo["file_path"],
        ]; */
        
        return array("ret"=>200,'data'=>array("url"=>$result['filepath']),'msg'=>'');
	}
}

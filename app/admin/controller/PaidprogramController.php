<?php

/**
 * 付费内容列表
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class PaidprogramController extends AdminbaseController {

    protected function getType($k=''){
        $type=[
            '0'=>lang('SINGLE_VIDEO'),
            '1'=>lang('MULTI_VIDEO'),
        ];
        if($k===''){
            return $type;
        }
        return $type[$k] ?? '';
    }

    protected function getStatus($k=''){
        $mark=[
            '-1'=>lang('REFUSE'),
            '0'=>lang('UNDER_REVIEW'),
            '1'=>lang('AGREE'),
        ];
        if($k===''){
            return $mark;
        }
        return $mark[$k] ?? '';
    }

    protected function getOrderType($k=''){
        $type=[
            '1'=>lang('ALIPAY'),
            '2'=>lang('WECHAT'),
            '3'=>lang('BALANCE'),
            '4'=>lang('WECHAT_MINI_PROGRAM'),
            '5'=>lang('PAYPAL'),
            '6'=>lang('BRAIN_TREE_PAYPAL'),
        ];
        if($k===''){
            return $type;
        }
        return $type[$k] ?? '';
    }

    protected function getOrderStatus($k=''){
        $mark=[
            '0'=>'未支付',
            '1'=>'已支付',
        ];

        if($k===''){
            return $mark;
        }

        return $mark[$k] ?? '';
    }

    function applylist(){

        $data = $this->request->param();
        $map=[];
        
        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }

        $uid=$data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }

        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        $lists = Db::name("paidprogram_apply")
            ->where($map)
            ->order("addtime desc")
            ->paginate(20);

        $lists->each(function($v,$k){
            $v['userinfo']=getUserInfo($v['uid']);
            return $v;           
        });

        $page = $lists->render();
        $this->assign('lists', $lists);
        $this->assign('page', $page);
        $this->assign('status', $this->getStatus());

        return $this->fetch();
    }

    //申请编辑
    function applyedit(){
        $id = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('paidprogram_apply')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }

        $data['userinfo']=getUserInfo($data['uid']);
        
        $this->assign('status',$this->getStatus());
        $this->assign('data', $data);
        return $this->fetch();
    }

    //申请编辑提交
    function applyeditPost(){
        if ($this->request->isPost()){
            $data = $this->request->param();
            $percent=$data['percent'];

            if($percent<0||$percent>100){
                $this->error(lang('PERCENTAGE_SHOULD_BE_BETWEEN_0_AND_100'));
            }

            if(floor($percent)!=$percent){
                $this->error(lang('PERCENTAGE_MUST_BE_INTEGER'));
            }

            $data['uptime']=time();

            $rs = DB::name('paidprogram_apply')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
			
			
			$action="修改付费内容申请列表ID: ".$data['id'];
			setAdminLog($action);
            
            $this->success(lang("MODIFICATION_SUCCESSFUL"));

        }

    }


    //申请删除
    function applydel(){
        $id = $this->request->param('id', 0, 'intval');
        $rs = DB::name('paidprogram_apply')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
		
		$action="删除付费内容申请列表ID: ".$id;
		setAdminLog($action);

        $this->success(lang("DELETE_SUCCESS"));
    }
    
    //付费内容列表
    function index(){
        $data = $this->request->param();
        $map=[];

         $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        $uid=$data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }

        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }

    	$lists = Db::name("paidprogram")
            ->where($map)
			->order("addtime desc")
			->paginate(20);
        
        $lists->each(function($v,$k){
            $v['userinfo']=getUserInfo($v['uid']);
			$v['thumb']=get_upload_path($v['thumb']);

            $video_arr=json_decode($v['videos'],true);
            foreach ($video_arr as $k1 => $v1) {
                $video_arr[$k1]['video_url']=get_upload_path($v1['video_url']);
            }

            $v['video_arr']=$video_arr;

            if($v['evaluate_nums']==0){
                $v['evaluate_point']=0;
            }else{
                $v['evaluate_point']=floor($v['evaluate_total']/$v['evaluate_nums']);
            }

            return $v;           
        });
        
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	$this->assign("status", $this->getStatus());
        $this->assign("type", $this->getType());
    	return $this->fetch();
    }

    //付费内容视频观看
    function videoplay(){
        $data=$this->request->param();
        $url=$data['url'];
        $this->assign("url", $url);
        return $this->fetch();
    }
    
    //删除付费内容
	function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('paidprogram')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        $action="删除付费内容ID：{$id}";
        setAdminLog($action);
        
        //删除评论内容         
        Db::name("paidprogram_comment")->where("object_id={$id}")->delete();
        //修改付费项目订单
        Db::name("paidprogram_order")->where("object_id={$id}")->update(array('isdel'=>1));

        //修改视频的绑定信息
        Db::name("video")->where("type=2 and goodsid={$id}")->update(array('type'=>0,'goodsid'=>0));

        $this->success(lang("DELETE_SUCCESS"));
	}
    
    //编辑付费内容
    function edit(){

        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('paidprogram')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }

        $data['userinfo']=getUserInfo($data['uid']);
        $data['thumb']=get_upload_path($data['thumb']);

        if($data['evaluate_nums']==0){
            $data['evaluate_point']=0;
        }else{
            $data['evaluate_point']=floor($data['evaluate_total']/$data['evaluate_nums']);
        }

        $video_arr=json_decode($data['videos'],true);
        foreach ($video_arr as $k1 => $v1) {
            $video_arr[$k1]['video_url']=get_upload_path($v1['video_url']);
        }

        $data['video_arr']=$video_arr;
        
        $this->assign("type", $this->getType());
    	$this->assign("status", $this->getStatus());        
        $this->assign('data', $data);
        return $this->fetch();            
    }
    
	function editPost(){
		if ($this->request->isPost()) {
            
            $data= $this->request->param();

            $id=$data['id'];
            $status=$data['status'];
            $uid=$data['uid'];

            //判断付费内容发布者是否注销
            $is_destroy=checkIsDestroy($uid);
            if($is_destroy){
                $this->error(lang('USER_DEACTIVATED_PAID_CONTENT_STATUS_CANNOT_BE_CHANGED'));
            }
                        
			$info = DB::name('paidprogram')->find($id);
			$rs = DB::name('paidprogram')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }

			if($info['status']!=$status){
				$action="修改付费内容ID：{$id} 审核状态：".$this->getStatus($status);
				setAdminLog($action);
			}

            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}	
	}
    
    //付费内容订单
    function orderlist(){
        $data = $this->request->param();
        $map=[];
        
        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }

        $type= $data['type'] ?? '';
        if($type!=''){
            $map[]=['type','=',$type];
        }
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        $uid=$data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['orderno|trade_no','like',"%".$keyword."%"];
        }
        
        $lists = DB::name("paidprogram_order")
            ->where($map)
            ->order('id desc')
            ->paginate(20);
        
        $lists->each(function($v,$k){
            $v['userinfo']=getUserInfo($v['uid']);
            $v['touserinfo']=getUserInfo($v['touid']);
            $v['object_name']=Db::name("paidprogram")->where("id={$v['object_id']}")->value("title");
            return $v;
        });
        
        $lists->appends($data);
        $page = $lists->render();
        
        $this->assign('lists', $lists);
        $this->assign('type', $this->getOrderType());
        $this->assign('status', $this->getOrderStatus());
        $this->assign("page", $page);
        return $this->fetch();
    }

    //确认支付
    function setPay(){
        $id = $this->request->param('id', 0, 'intval');
        if($id){
            $orderinfo=Db::name("paidprogram_order")->where(["id"=>$id,"status"=>0])->find();
            if($orderinfo){

                $now=time();

                /* 更新 订单状态 */
                $data['status']=1;
                $data['edittime']=$now;
                
                Db::name("paidprogram_order")->where("id='{$orderinfo['id']}'")->update($data);

                $uid=$orderinfo['uid'];
                $touid=$orderinfo['touid'];
                $object_id=$orderinfo['object_id'];

                //获取用户的商城累计消费
                $balance_consumption=Db::name("user")->where("id={$uid}")->value("balance_consumption");

                //增加用户的商城累计消费
                Db::name("user")->where("id={$uid}")->inc('balance_consumption',$orderinfo['money'])->update();

                //增加付费内容的销量
                Db::name("paidprogram")->where("id={$object_id}")->inc('sale_nums')->update();

                //给付费内容作者增加余额
                $apply_info=Db::name("paidprogram_apply")->where("uid={$touid}")->find();
                $percent=$apply_info['percent'];

                $balance=$orderinfo['money'];

                if($percent>0){
                    $balance=$balance*(100-$percent)/100;
                    $balance=round($balance,2);
                }

                //给发布者增加余额
                setUserBalance($touid,1,$balance);

                $data1=array(
                    'uid'=>$touid,
                    'touid'=>$uid,
                    'balance'=>$balance,
                    'type'=>1,
                    'action'=>8, //付费内容收入
                    'orderid'=>$id,
                    'addtime'=>$now
                );

                addBalanceRecord($data1);

				$action="确认支付付费内容：{$id}";
				setAdminLog($action);

                $this->success(lang('CONFIRM_PAYMENT_SUCCESS'));
            }else{
                $this->error(lang('DATA_TRANSFER_FAILED'));
            }
        }else{
            $this->error(lang('DATA_TRANSFER_FAILED'));
        }
    }

    //导出订单记录
    function export(){

        $data = $this->request->param();
        $map=[];
        
        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }

        $type=$data['type'] ?? '';
        if($type!=''){
            $map[]=['type','=',$type];
        }
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $uid= $data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['orderno|trade_no','like',"%".$keyword."%"];
        }
        
        $xlsName  = lang('PAID_CONTENT_ORDER');
        
        $xlsData=DB::name("paidprogram_order")
            ->where($map)
            ->order('id desc')
            ->select()
            ->toArray();

        if(empty($xlsData)){
            $this->error(lang("DATA_EMPTY"));
        }

        foreach ($xlsData as $k => $v){

            $userinfo=getUserInfo($v['uid']);
            $touserinfo=getUserInfo($v['touid']);
            $xlsData[$k]['user_nickname']= $userinfo['user_nickname']."(".$v['uid'].")";
            $xlsData[$k]['touser_nickname']= $touserinfo['user_nickname']."(".$v['touid'].")";
            $xlsData[$k]['addtime']=date("Y-m-d H:i:s",$v['addtime']); 
            $xlsData[$k]['edittime']=$v['edittime']>0?date("Y-m-d H:i:s",$v['edittime']):''; 
            $xlsData[$k]['status']=$this->getOrderStatus($v['status']);
            $xlsData[$k]['type']=$this->getOrderType($v['type']);
            $xlsData[$k]['object_name']=Db::name("paidprogram")->where("id={$v['object_id']}")->value("title");
        }

        $action="导出付费内容订单列表：".DB::name("paidprogram_order")->getLastSql();
        setAdminLog($action);
        $cellName = array('A','B','C','D','E','F','G','H','I','J','K');
        $xlsCell  = array(
            array('id', lang('SERIAL_NUMBER')),
            array('user_nickname', lang('PURCHASING_USER')),
            array('touser_nickname', lang('PUBLISH_USER')),
            array('object_name', lang('PAID_CONTENT_TITLE')),
            array('type', lang('PAYMENT_METHOD')),
            array('money', lang('AMOUNT')),
            array('orderno', lang('ORDER_NUMBER')),
            array('trade_no', lang('THIRD_PARTY_PAYMENT_ORDER_NUMBER')),
            array('status', lang('ORDER_STATUS')),
            array('addtime', lang('SUBMISSION_TIME')),
            array('edittime', lang('PROCESSING_TIME')),
        );
        exportExcel($xlsName,$xlsCell,$xlsData,$cellName);
    }
   
}

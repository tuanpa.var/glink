<?php

/**
 * 付费内容分类
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class PaidprogramclassController extends AdminbaseController {

    protected function getStatus($k=''){
        $type=[
            '0'=>lang('NOT_DISPLAYED'),
            '1'=>'显示',
        ];
        if($k===''){
            return $type;
        }
        return $type[$k] ?? '';
    }

    /*分类列表*/
    function index(){
        $data = $this->request->param();
        $map=[];

        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['name','like','%'.$keyword.'%'];
        }

        $lists = Db::name("paidprogram_class")
                ->where($map)
                ->order("list_order asc,id DESC")
                ->paginate(20);

        $lists->appends($data);
        $page = $lists->render();

        $this->assign('lists', $lists);
        $this->assign('status', $this->getStatus());
        $this->assign("page", $page);
        
        return $this->fetch();
    }


    //分类排序
    function listOrder() { 
        $model = DB::name('paidprogram_class');
        parent::listOrders($model);
        
        $this->resetcache();

		$action="更新付费内容分类列表排序 ";
        setAdminLog($action);

        $this->success(lang('SORT_UPDATE_SUCCESS'));
    }


    /*分类删除*/
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('paidprogram_class')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }

        $this->resetcache();

		$action="删除付费内容分类列表ID: ".$id;
        setAdminLog($action);

        Db::name("paidprogram")->where("classid={$id}")->update(array("classid"=>0));
        $this->success(lang("DELETE_SUCCESS"));
    }

    /*分类添加*/
    function add(){
        $this->assign("status", $this->getStatus());
        return $this->fetch();
    }

    /*分类添加提交*/
    function addPost(){
        if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
            $name=trim($data['name']); //去除左右两边空格
            $name_en=trim($data['name_en']); //去除左右两边空格

            if($name==""){
                $this->error(lang('PLEASE_FILL_IN_THE_CHINESE_CATEGORY_NAME'));
            }
            
            $isexist=DB::name('paidprogram_class')->where(['name'=>$name])->find();
            if($isexist){
                $this->error(lang('CHINESE_CATEGORY_NAME_ALREADY_EXISTS'));
            }

            if($name_en==""){
                $this->error(lang('PLEASE_FILL_IN_THE_CATEGORY_ENGLISH_NAME'));
            }
            
            $isexist=DB::name('paidprogram_class')->where(['name_en'=>$name_en])->find();
            if($isexist){
                $this->error(lang('CATEGORY_ENGLISH_NAME_ALREADY_EXISTS'));
            }
            
            $data['addtime']=time();
            
            $id = DB::name('paidprogram_class')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
			
			$action="添加付费内容分类列表ID: ".$id;
			setAdminLog($action);

            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
        }
    }

    /*分类编辑*/
    function edit(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('paidprogram_class')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign('status',$this->getStatus());
        $this->assign('data', $data);
        return $this->fetch();
    }

    /*分类编辑提交*/
    function editPost(){
        if ($this->request->isPost()){
            
            $data = $this->request->param();
            
            $name=trim($data['name']);  //去除左右两边空格
            $name_en=trim($data['name_en']);  //去除左右两边空格
            $id=$data['id'];

            if($name==""){
                $this->error(lang('PLEASE_FILL_IN_THE_CATEGORY_NAME'));
            }
            
            $isexist=DB::name('paidprogram_class')->where([['id','<>',$id],['name','=',$name]])->find();
            if($isexist){
                $this->error(lang('CATEGORY_NAME_ALREADY_EXISTS'));
            }
            
            if(mb_strlen($name)>30){
                $this->error(lang('WORD_COUNT_NOT_EXCEED_30'));
            }

            if($name_en==""){
                $this->error(lang('PLEASE_FILL_IN_THE_CATEGORY_ENGLISH_NAME'));
            }
            
            $isexist=DB::name('paidprogram_class')->where([['id','<>',$id],['name_en','=',$name_en]])->find();
            if($isexist){
                $this->error(lang('CATEGORY_ENGLISH_NAME_ALREADY_EXISTS'));
            }
            
            if(mb_strlen($name_en)>100){
                $this->error(lang('WORD_COUNT_NOT_EXCEED_100'));
            }

            $data['edittime']=time();
            
            $rs = DB::name('paidprogram_class')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }

			$action="修改付费内容分类列表ID: ".$id;
			setAdminLog($action);

            $this->resetcache();
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
        }

    }


    // 写入付费项目分类缓存
    function resetcache(){
        $key='getPaidClass';
        
        $rs=DB::name('paidprogram_class')
            ->field("id,name,name_en")
            ->where('status=1')
            ->order("list_order asc,id desc")
            ->select();
        if($rs){
            setcaches($key,$rs);
        }   
        return 1;
    }
}

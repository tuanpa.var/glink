<?php

/**
 * 退款理由
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class RefundreasonController extends AdminbaseController {

    protected function getStatus($k=''){
        $status=array(
            '0'=>lang('HIDE'),
            '1'=>lang('DISPLAY'),
        );
        if($k===''){
            return $status;
        }
        return $status[$k] ?? '';
    }
    
    /*分类列表*/
	function index(){
        $data = $this->request->param();
        $map=[];

        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['name','like','%'.$keyword.'%'];
        }

    	$lists = Db::name("shop_refund_reason")
                ->where($map)
                ->order("list_order asc,id DESC")
                ->paginate(20);

        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	$this->assign("status", $this->getStatus());
    	return $this->fetch();
	}
    
    //分类排序
    function listOrder() { 
        $model = DB::name('shop_refund_reason');
        parent::listOrders($model);
        
        $this->resetcache();

		$action="更新买家退款原因列表排序";
        setAdminLog($action);

        $this->success(lang('SORT_UPDATE_SUCCESS'));
    }


	/*分类删除*/
	function del(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('shop_refund_reason')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }

        $this->resetcache();
		
		$action="删除买家退款原因列表ID: ".$id;
        setAdminLog($action);
        
        $this->success(lang("DELETE_SUCCESS"));
	}


	/*分类添加*/
	function add(){
        $this->assign("status", $this->getStatus());
		return $this->fetch();
	}

	/*分类添加提交*/
	function addPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$name=$data['name'];
            $name_en=$data['name_en'];

			if($name==""){
				$this->error(lang('PLEASE_FILL_IN_REFUND_REASON'));
			}

            if($name_en==""){
                $this->error(lang('PLEASE_FILL_IN_REFUND_REASON_ENGLISH'));
            }
            
            $isexist=DB::name('shop_refund_reason')->where(['name'=>$name])->find();
            if($isexist){
                $this->error(lang('REFUND_REASON_CHINESE_ALREADY_EXISTS'));;
            }

            $isexist=DB::name('shop_refund_reason')->where(['name_en'=>$name_en])->find();
            if($isexist){
                $this->error(lang('REFUND_REASON_ENGLISH_ALREADY_EXISTS'));
            }
            
            $data['addtime']=time();
            
			$id = DB::name('shop_refund_reason')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }

			$action="添加买家退款原因列表ID: ".$id;
			setAdminLog($action);

            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
		}

	}

	/*分类编辑*/
	function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('shop_refund_reason')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign('status',$this->getStatus());
        $this->assign('data', $data);
        return $this->fetch();
	}

	/*分类编辑提交*/
	function editPost(){
        if ($this->request->isPost()){
            
            $data = $this->request->param();
            
			$name=$data['name'];
            $name_en=$data['name_en'];
			$id=$data['id'];

			if($name==""){
				$this->error(lang('PLEASE_FILL_IN_REFUND_REASON'));
			}

            if($name_en==""){
                $this->error(lang('PLEASE_FILL_IN_REFUND_REASON_ENGLISH'));
            }
            
            $isexist=DB::name('shop_refund_reason')->where([['id','<>',$id],['name','=',$name]])->find();
            if($isexist){
                $this->error(lang('REFUND_REASON_CHINESE_ALREADY_EXISTS'));;
            }
            
            if(mb_strlen($name)>30){
                $this->error(lang('CHINESE_CHARACTERS_NOT_EXCEED_30'));
            }

            $isexist=DB::name('shop_refund_reason')->where([['id','<>',$id],['name_en','=',$name_en]])->find();
            if($isexist){
                $this->error(lang('REFUND_REASON_ENGLISH_ALREADY_EXISTS'));
            }

            if(mb_strlen($name)>100){
                $this->error(lang('ENGLISH_WORDS_SHOULD_NOT_EXCEED_100_CHARACTERS'));
            }

            $data['edittime']=time();
            
			$rs = DB::name('shop_refund_reason')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }

			$action="修改买家退款原因列表ID: ".$id;
			setAdminLog($action);
			
            $this->resetcache();
            
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
            
		}

	}


    // 写入退款原因缓存
    function resetcache(){
        $key='getRefundReason';
        
        $rs=DB::name('shop_refund_reason')
            ->field("id,name,name_en")
            ->where('status=1')
            ->order("list_order asc,id desc")
            ->select();
        if($rs){
            setcaches($key,$rs);
        }   
        return 1;
    }

}

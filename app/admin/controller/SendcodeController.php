<?php

/**
 * 验证码管理
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class SendcodeController extends AdminbaseController {
    
    protected function getTypes($k=''){
        $type=array(
            '1'=>lang('SMS_VERIFICATION_CODE'),
            '2'=>lang('EMAIL_VERIFICATION_CODE'),
        );
        if($k===''){
            return $type;
        }
        
        return $type[$k] ?? '';
    }

    protected function getSendTypes($k=''){
        $type=array(
            '0'=> lang('UNKNOWN'),
            '1'=> lang('ALIBABA_CLOUD'),
            '2'=> lang('RONGLIANYUN'),
            '3'=> lang('TENCENT_CLOUD'),
        );
        if($k===''){
            return $type;
        }
        
        return $type[$k] ?? '';
    }
    
    function index(){

        $data = $this->request->param();
        $map=[];
		
        $type= $data['type'] ?? '';
        if($type!=''){
            $map[]=['type','=',$type];
        }
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['account','like',"%".$keyword."%"];
        }
        
    	$lists = DB::name("sendcode")
            ->where($map)
            ->order('id desc')
            ->paginate(20);
        
        $lists->each(function($v,$k){
            $v['account']=m_s($v['account']);
            return $v;
        });
        
        $lists->appends($data);
        $page = $lists->render();
        
        $this->assign('lists', $lists);
    	$this->assign('type', $this->getTypes());
        $this->assign('sendType', $this->getSendTypes());
    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }
    
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        if($id){
            $result=DB::name("sendcode")->delete($id);				
            if($result){
                $this->success(lang('DELETE_SUCCESS'));
             }else{
                $this->error(lang('DELETE_FAILED'));
             }
        }else{				
            $this->error(lang('DATA_TRANSFER_FAILED'));
        }				
    }	
    
    function export(){
        
        $data = $this->request->param();
        $map=[];
		
        $type= $data['type'] ?? '';
        if($type!=''){
            $map[]=['type','=',$type];
        }
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $keyword= $data['keyword'] ?? '';
        if($keyword!=''){
            $map[]=['account','like',"%".$keyword."%"];
        }
        
        $xlsName  = "验证码";
        
    	$xlsData = DB::name("sendcode")
            ->where($map)
            ->order('id desc')
            ->select()
            ->toArray();

        if(empty($xlsData)){
            $this->error(lang("DATA_EMPTY"));
        }

        foreach ($xlsData as $k => $v){

            $xlsData[$k]['account']=m_s($v['account']);
            $xlsData[$k]['msg_type']=$this->getTypes($v['type']);
            $xlsData[$k]['send_type']=$this->getSendTypes($v['send_type']);
            $xlsData[$k]['addtime']=date("Y-m-d H:i:s",$v['addtime']);             
        }

		$action="验证码管理列表：".Db::name("sendcode")->getLastSql();
        setAdminLog($action);
        $cellName = array('A','B','C','D','E','F');
        $xlsCell  = array(
            array('id', lang('SERIAL_NUMBER')),
            array('msg_type', lang('MESSAGE_TYPE')),
            array('account', lang('RECEIVING_ACCOUNT')),
            array('content', lang('MESSAGE_CONTENT')),
            array('addtime', lang('SUBMISSION_TIME')),
            array('send_type', lang('SEND_TYPE')),
        );
        exportExcel($xlsName,$xlsCell,$xlsData,$cellName);
    }
    
}

<?php

/**
 * 贴纸礼物
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class StickerController extends AdminbaseController {
    protected function getTypes($k=''){
        $type=[
            '2'=>lang('STICKER_GIFT'),
        ];
        if($k===''){
            return $type;
        }
        return $type[$k] ?? '';
    }
    protected function getMark($k=''){
        $mark=[
            '0'=>lang('ORDINARY'),
            '1'=>lang('HOT'),
            '2'=>lang('GUARDIAN'),
        ];
        if($k===''){
            return $mark;
        }
        return $mark[$k] ?? '';
    }
    
    protected function getSwftype($k=''){
        $swftype=[
            '0'=>'GIF',
            '1'=>'SVGA',
        ];
        if($k===''){
            return $swftype;
        }
        return $swftype[$k] ?? '';
    }
    
    function index(){

    	$lists = Db::name("gift")
            ->where('type=2')
			->order("list_order asc,id desc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['gifticon']=get_upload_path($v['gifticon']);
			$v['swf']=get_upload_path($v['swf']);
            return $v;           
        });
        
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	$this->assign("type", $this->getTypes());
    	$this->assign("mark", $this->getMark());
    	$this->assign("swftype", $this->getSwftype());
    	
    	return $this->fetch();
    }
    
	function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('gift')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        $action="删除贴纸礼物：{$id}";
        setAdminLog($action);
                    
        $this->resetcache();
        $this->success(lang("DELETE_SUCCESS"));
	}
    
    //排序
    public function listOrder() { 
		
        $model = DB::name('gift');
        parent::listOrders($model);
        
        $action="更新贴纸礼物排序";
        setAdminLog($action);
        
        $this->resetcache();
        $this->success(lang('SORT_UPDATE_SUCCESS'));
    }

    function add(){
        $this->assign("type", $this->getTypes());
    	$this->assign("mark", $this->getMark());
    	$this->assign("swftype", $this->getSwftype());
        return $this->fetch();				
    }

	function addPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
            $giftname=$data['giftname'];
            $giftname_en=$data['giftname_en'];

            if($giftname == ''){
                $this->error(lang('PLEASE_ENTER_CHINESE_NAME'));
            }else{
                $check = Db::name('gift')->where("giftname='{$giftname}'")->find();
                if($check){
                    $this->error(lang('NAME_ALREADY_EXISTS'));
                }
            }

            if($giftname_en == ''){
                $this->error(lang("PLEASE_ENTER_ENGLISH_NAME"));
            }else{
                $check = Db::name('gift')->where("giftname_en='{$giftname_en}'")->find();
                if($check){
                    $this->error(lang('NAME_ALREADY_EXISTS_IN_ENGLISH'));
                }
            }

            $needcoin=$data['needcoin'];
            $gifticon=$data['gifticon'];
            $sticker_id=$data['sticker_id'];
            $swftime=$data['swftime'];
            
            if($needcoin==''){
                $this->error(lang('PLEASE_ENTER_PRICE'));
            }

            if($gifticon==''){
                $this->error(lang('PLEASE_UPLOAD_IMAGE'));
            }

            $data['gifticon']=set_upload_path($gifticon);
            
            if($sticker_id==''){
                $this->error(lang('PLEASE_FILL_IN_STICKER_ID'));
            }

            if($swftime==''){
                $this->error(lang('PLEASE_FILL_IN_ANIMATION_DURATION'));
            }

            $data['addtime']=time();
            
			$id = DB::name('gift')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            
            $action="添加贴纸礼物：{$id}";
            setAdminLog($action);
            
            $this->resetcache();
            $this->success(lang('ADD_SUCCESS'));
		}			
	}
    
    function edit(){

        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('gift')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        
        $this->assign("type", $this->getTypes());
    	$this->assign("mark", $this->getMark());
    	$this->assign("swftype", $this->getSwftype());
        $this->assign('data', $data);
        return $this->fetch();            
    }
    
	function editPost(){
		if ($this->request->isPost()) {
            
            $data  = $this->request->param();

            $id=$data['id'];
            $giftname=$data['giftname'];
            $giftname_en=$data['giftname_en'];
            if($giftname == ''){
                $this->error(lang('PLEASE_ENTER_CHINESE_NAME'));
            }else{
                $check = Db::name('gift')->where("giftname='{$giftname}' and id!={$id}")->find();
                if($check){
                    $this->error(lang('NAME_ALREADY_EXISTS_IN_CHINESE'));
                }
            }

            if($giftname_en == ''){
                $this->error(lang("PLEASE_ENTER_ENGLISH_NAME"));
            }else{
                $check = Db::name('gift')->where("giftname_en='{$giftname_en}' and id!={$id}")->find();
                if($check){
                    $this->error(lang('NAME_ALREADY_EXISTS_IN_ENGLISH'));
                }
            }

            $needcoin=$data['needcoin'];
            $gifticon=$data['gifticon'];
            $sticker_id=$data['sticker_id'];
            
            if($needcoin==''){
                $this->error(lang('PLEASE_ENTER_PRICE'));
            }

            if($gifticon==''){
                $this->error(lang('PLEASE_UPLOAD_IMAGE'));
            }

            $gifticon_old=$data['gifticon_old'];
            if($gifticon!=$gifticon_old){
                $data['gifticon']=set_upload_path($data['gifticon']);
            }
            
            if($sticker_id==''){
                $this->error(lang('PLEASE_FILL_IN_STICKER_ID'));
            }
            
            unset($data['gifticon_old']);
            
			$rs = DB::name('gift')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            $action="修改贴纸礼物：{$data['id']}";
            setAdminLog($action);
            $this->resetcache();
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}	
	}
        
    function resetcache(){
        $key='getPropgiftList';
        
		$rs=DB::name('gift')
			->field("id,type,mark,giftname,giftname_en,needcoin,gifticon,sticker_id,swftime,isplatgift")
            ->where('type=2')
			->order("list_order asc,id desc")
			->select();
        if($rs){
            setcaches($key,$rs);
        }else{
			delcache($key);
		}
        return 1;
    }
}

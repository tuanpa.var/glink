<?php

/**
 * 直播间系统消息
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;

class SystemController extends AdminbaseController {

    function index(){
		$config=getConfigPri();
		$this->assign('config', $config);
    	return $this->fetch('edit');
    }

    function send(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
			$content=$data['content'];
			if($content==''){
				$this->error(lang('CONTENT_CANNOT_BE_EMPTY'));
			}
            $action="发送系统消息：{$content}";
            setAdminLog($action);
            $this->success(lang('SEND_SUCCESS'));
		}
	}		
}

<?php

/**
 * 短视频-举报
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class VideorepController extends AdminbaseController {

    protected function getStatus($k=''){
        $status=array(
            '0'=>lang('UNPROCESSED'),
            '1'=>lang('PROCESSED'),
        );
        if($k===''){
            return $status;
        }
        
        return $status[$k] ?? '';
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
		
        $status= $data['status'] ?? '';
        if($status!=''){
            $map[]=['status','=',$status];
        }
        
        $start_time= $data['start_time'] ?? '';
        $end_time= $data['end_time'] ?? '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        $uid=$data['uid'] ?? '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                
                array_push($lianguid,$uid);
                $map[]=['uid','in',$lianguid];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $lists = DB::name("video_report")
            ->where($map)
            ->order('id desc')
            ->paginate(20);
        
        $lists->each(function($v,$k){
            $v['userinfo']=getUserInfo($v['uid']);
            $v['touserinfo']=getUserInfo($v['touid']);
            $v['content']=nl2br($v['content']);
            //判断视频是否下架
            $isdel=Db::name("video")->where("id={$v['videoid']}")->value("isdel");
            isset($isdel)?$v['isdel']=$isdel:$v['isdel']=0;
            return $v;
        });
        
        $lists->appends($data);
        $page = $lists->render();
        
        $this->assign('lists', $lists);
    	$this->assign('status', $this->getStatus());
    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }
    
    public function setstatus(){

        $id = $this->request->param('id', 0, 'intval');
        if(!$id){
            $this->error(lang('DATA_TRANSFER_FAILED'));
        }
        
        $nowtime=time();
        
        $rs=DB::name("video_report")->where("id={$id}")->update(['status'=>1,'uptime'=>$nowtime]);
        if($rs===false){
            $this->error(lang('OPERATION_FAILED'));
        }
		
		$action="视频管理-标记处理举报列表ID: ".$id;
		setAdminLog($action);
        $this->success(lang('OPERATION_SUCCESS'));        
    }

    function del(){
        $id = $this->request->param('id', 0, 'intval');
        if($id){
            $result=DB::name("video_report")->delete($id);				
            if($result){
				$action="视频管理-删除举报列表ID: ".$id;
				setAdminLog($action);
                $this->success(lang('DELETE_SUCCESS'));
             }else{
                $this->error(lang('DELETE_FAILED'));
             }
        }else{				
            $this->error(lang('DATA_TRANSFER_FAILED'));
        }				
    }

}

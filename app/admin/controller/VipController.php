<?php

/**
 * VIP管理
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;

class VipController extends AdminbaseController {

    protected function getLong($k=''){
        $long=array(
            '1'=>lang('ONE_MONTH'),
            '3'=>lang('THREE_MONTHS'),
            '6'=>lang('SIX_MONTHS'),
            '12'=>lang('TWELVE_MONTHS'),
        );
        if($k===''){
            return $long;
        }
        return $long[$k];
    }

    function index(){
        $lists = Db::name("vip")
			->order("list_order asc")
			->paginate(20);
        
        $page = $lists->render();

    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
        $this->assign('long', $this->getLong());
    	
    	return $this->fetch();
    }

	function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('vip')->where("id={$id}")->delete();
        if(!$rs){
            $this->error(lang("DELETE_FAILED"));
        }
        
        $action="删除VIP：{$id}";
        setAdminLog($action);
        
        $this->success(lang('DELETE_SUCCESS'),url("vip/index"));
            
	}
	
    //排序
    public function listOrder() { 
		
        $model = DB::name('vip');
        parent::listOrders($model);
        $action="更新VIP排序";
        setAdminLog($action);
        
        $this->success(lang('SORT_UPDATE_SUCCESS'));
    }    

	function add(){
        $this->assign('long', $this->getLong());
        return $this->fetch();				
	}
    
    function addPost(){
		if ($this->request->isPost()) {
            
            $configpub=getConfigPub();
            $data = $this->request->param();
            
			$length=$data['length'];
            
            $isexist=DB::name('vip')->where(['length'=>$length])->find();
			
			if($isexist){
                $this->error(lang('DURATION_SETTING_ALREADY_EXISTS_FOR_SAME_TYPE'));
			}
            
            $coin=$data['coin'];
			if($coin==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED').$configpub['name_coin']);
			}

			if(!is_numeric($coin)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED').$configpub['name_coin']);
			}

            if($coin<1||$coin>99999999){
                $this->error(lang('REQUIRED').$configpub['name_coin']."必须在1-99999999之间");
            }
            
            $score=$data['score'];
			if($score==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED').$configpub['name_score']);
			}

			if(!is_numeric($score)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED').$configpub['name_score']);
			}

            if($score<1||$score>99999999){
                $this->error(lang('REQUIRED').$configpub['name_score']."必须在1-99999999之间");
            }
            
            $data['addtime']=time();
            
			$id = DB::name('vip')->insertGetId($data);
            if(!$id){
                $this->error(lang('ADD_FAILED'));
            }
            
            $action="添加VIP：{$id}";
            setAdminLog($action);
            $this->success(lang('ADD_SUCCESS'));
		}			
	}

	function edit(){        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('vip')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error(lang("INFORMATION_ERROR"));
        }
        $this->assign('data', $data);
        $this->assign('long', $this->getLong());
        
        return $this->fetch();
	}
    
    function editPost(){
		if ($this->request->isPost()) {
            
            $configpub=getConfigPub();
            
            $data = $this->request->param();
            
            /* $length=$data['length'];
            
            $isexist=DB::name('vip')->where(['length'=>$length])->find();
			
			if($isexist){
                $this->error(lang('DURATION_SETTING_ALREADY_EXISTS_FOR_SAME_TYPE'));
			} */
            
            $coin=$data['coin'];
			if($coin==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED').$configpub['name_coin']);
			}

			if(!is_numeric($coin)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED').$configpub['name_coin']);
			}

            if($coin<1||$coin>99999999){
                $this->error(lang('REQUIRED').$configpub['name_coin']."必须在1-99999999之间");
            }
            
            $score=$data['score'];
			if($score==""){
				$this->error(lang('PLEASE_FILL_IN_THE_REQUIRED').$configpub['name_score']);
			}

			if(!is_numeric($score)){
				$this->error(lang('PLEASE_CONFIRM_THE_REQUIRED').$configpub['name_score']);
			}

            if($score<1||$score>99999999){
                $this->error(lang('REQUIRED').$configpub['name_score']."必须在1-99999999之间");
            }
            
			$rs = DB::name('vip')->update($data);
            if($rs===false){
                $this->error(lang("MODIFICATION_FAILED"));
            }
            
            $action="修改VIP：{$data['id']}";
            setAdminLog($action);
            $this->success(lang("MODIFICATION_SUCCESSFUL"));
		}
	}
	
}

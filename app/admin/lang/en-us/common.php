<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-present http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
return [
    'CLOSE' => 'Close',
    'WARN'  => 'Warn',
    'BAN'   => 'Ban',
    'ORDER' => 'Order',
    'PRICE' => 'Price',
    'TIMES'  => 'Times',
    'TIME'  => 'Time',
    'INTEGER' => 'Integer',
    'PLEASE_FILL_IN_A_NUMBER' => 'Please fill in a number',
    'OPERATION_FAILED_PLEASE_RETRY' => 'Operation failed, please retry',
    'PLEASE_ENTER_REASON_FOR_REMOVAL' => 'Please enter reason for removal',
    'ARE_YOU_SURE_YOU_WANT_TO_DELETE' => 'Are you sure you want to delete?',
    'ARE_YOU_SURE_YOU_WANT_TO_CANCEL_THIS_TAG_RECOMMENDATION' => 'Are you sure you want to cancel this tag recommendation?',
    'ARE_YOU_SURE_YOU_WANT_TO_THIS_TAG_RECOMMENDATION' => 'Are you sure you want to this tag recommendation?',
];

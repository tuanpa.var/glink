<?php
namespace app\admin\middleware;

use Closure;
use think\App;
use think\Config;
use think\Lang;
use think\Response;

class LoadSwitchLangPack
{
    protected $app;
    protected $lang;
    protected $config;

    public function __construct(App $app, Lang $lang, Config $config)
    {
        $this->app    = $app;
        $this->lang   = $lang;
        $this->config = $lang->getConfig();
    }

    public function handle($request, Closure $next): Response
    {
        // Call the next middleware and get the response
        $response = $next($request);

        if (empty(cmf_get_option('setting_language'))) {
            $settingLanguage = cmf_set_option('setting_language', ['lang' => config('lang.default_lang')]);
        } else {
            $settingLanguage = cmf_get_option('setting_language');
        }

        $this->app->cookie->set($this->config['cookie_var'], $settingLanguage['lang']);

        return $response;
    }
}
<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-present http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use app\admin\model\AdminMenuModel;
use think\Validate;

class AdminMenuValidate extends Validate
{
    protected $rule = [
        'name'       => 'require',
        'app'        => 'require',
        'controller' => 'require',
        'parent_id'  => 'checkParentId',
        'action'     => 'require|unique:AdminMenu,app^controller^action',
    ];

    protected $message = [
        'name.require'       => 'NAME_CANNOT_BE_EMPTY',
        'app.require'        => 'APPLICATION_CANNOT_BE_EMPTY',
        'parent_id'          => 'EXCEEDS_LEVEL_4',
        'controller.require' => 'NAME_CANNOT_BE_EMPTY',
        'action.require'     => 'NAME_CANNOT_BE_EMPTY',
        'action.unique'      => 'DUPLICATE_RECORD_EXISTS!',
    ];

    protected $scene = [
        'add'  => ['name', 'app', 'controller', 'action', 'parent_id'],
        'edit' => ['name', 'app', 'controller', 'action', 'id', 'parent_id'],

    ];

    // 自定义验证规则
    protected function checkParentId($value)
    {
        $find = AdminMenuModel::where("id", $value)->value('parent_id');

        if ($find) {
            $find2 = AdminMenuModel::where("id", $find)->value('parent_id');
            if ($find2) {
                $find3 = AdminMenuModel::where("id", $find2)->value('parent_id');
                if ($find3) {
                    return false;
                }
            }
        }
        return true;
    }
}

<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-present http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use app\admin\model\RouteModel;
use think\Validate;

class SettingSiteValidate extends Validate
{
    protected $rule = [
        'options.site_name'             => 'require',
        'admin_settings.admin_password' => 'alphaNum|checkAlias'
    ];

    protected $message = [
        'options.site_name.require'                => 'WEBSITE_NAME_CANNOT_BE_EMPTY',
        'admin_settings.admin_password.alphaNum'   => 'BACKEND_PASSWORD_CAN_ONLY_BE_ENGLISH_LETTERS_AND_NUMBERS',
        'admin_settings.admin_password.checkAlias' => 'THIS_PASSWORD_CANNOT_BE_USED',
    ];


    // 自定义验证规则
    protected function checkAlias($value, $rule, $data)
    {
        if (empty($value)) {
            return true;
        }

        if(preg_match('/^\d+$/',$value)){
            return lang('PASSWORD_CANNOT_BE_PURE_NUMBERS');
        }

        $routeModel = new RouteModel();
        $fullUrl    = $routeModel->buildFullUrl('admin/Index/index', []);
        if (!$routeModel->existsRoute($value.'$', $fullUrl)) {
            return true;
        } else {
            return lang('URL_RULE_ALREADY_EXISTS');
        }

    }

}

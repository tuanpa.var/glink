<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-present http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class UserValidate extends Validate
{
    protected $rule = [
        'user_login' => 'require|unique:user,user_login',
        'user_pass'  => 'require',
        'user_email' => 'require|email|unique:user,user_email',
    ];
    protected $message = [
        'user_login.require' => 'USER_NAME_CANNOT_BE_EMPTY',
        'user_login.unique'  => 'USER_NAME_ALREADY_EXISTS',
        'user_pass.require'  => 'PASSWORD_REQUIRED',
        'user_email.require' => 'EMAIL_CANNOT_BE_EMPTY',
        'user_email.email'   => 'EMAIL_FORMAT_IS_INCORRECT',
        'user_email.unique'  => 'EMAIL_ALREADY_EXISTS',
    ];

    protected $scene = [
        'add'  => ['user_login', 'user_pass', 'user_email'],
        'edit' => ['user_login', 'user_email'],
    ];
}

<?php
/**
 * 我的明细
 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\facade\Db;

class DetailController extends HomebaseController {

	function index(){       
		$data = $this->request->param();
        $uid= $data['uid'] ?? '';
        $token=$data['token'] ?? '';
        $uid=(int)checkNull($uid);
        $token=checkNull($token);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$reason=lang('YOUR_LOGIN_SESSION_HAS_EXPIRED_PLEASE_LOG_IN_AGAIN');
			$this->assign('reason', $reason);
			return $this->fetch(':error');
		}
        
		$this->assign("uid",$uid);
		$this->assign("token",$token);
		
		$list=Db::name('user_voterecord')->field("fromid,actionid,sum(nums) as num,sum(total) as totalall")->where(["action"=>'1',"uid"=>$uid])->group("fromid,actionid,showid")->order("addtime desc")->limit(0,50)->select()->toArray();
		//语言包
		$language_type=$this->language_type;

		foreach($list as $k=>$v){
			
			$giftinfo=Db::name('gift')->field("giftname,giftname_en")->where("id={$v['actionid']}")->find();
			if(!$giftinfo){
				$giftinfo=array(
					"giftname"=>lang('GIFT_DELETED'),
					'giftname_en'=>lang('GIFT_DELETED')
				);
			}

			if($language_type=='en'){
				$giftinfo['giftname']=$giftinfo['giftname_en'];
			}

			unset($giftinfo['giftname_en']);

			$list[$k]['giftinfo']=$giftinfo;
			$list[$k]['totalall']=number_format($v['totalall']);
			$userinfo=getUserInfo($v['fromid']);
			if(!$userinfo){
				$userinfo=array(
					"user_nickname"=>lang('USER_DELETED')
				);
			}
			$list[$k]['userinfo']=$userinfo;
		}
		
		$this->assign("list",$list);
		
		$list_live=Db::name('live_record')
            ->field("starttime,endtime")
            ->where(["uid"=>$uid])
            ->order("starttime desc")
            ->limit(0,50)
            ->select()
            ->toArray();
		foreach($list_live as $k=>$v){
            
			$cha=$v['endtime']-$v['starttime'];
			$list_live[$k]['length']=getSeconds($cha,1);
            $list_live[$k]['starttime']=date("Y-m-d H:i",$v['starttime']);
			$list_live[$k]['endtime']=date("Y-m-d H:i",$v['endtime']);
		}

		$this->assign("list_live",$list_live);
		
		return $this->fetch();
	    
	}
	
	public function receive_more()
	{
		$data = $this->request->param();
        $uid= $data['uid'] ?? '';
        $token=$data['token'] ?? '';
        $p=$data['page'] ?? '1';
        $uid=(int)checkNull($uid);
        $token=checkNull($token);
        $p=checkNull($p);
		
		$result=array(
			'data'=>array(),
			'nums'=>0,
			'isscroll'=>0,
		);
	
		if(checkToken($uid,$token)==700){
			echo json_encode($result);
            return;
		} 
		
		$pnums=50;
		$start=($p-1)*$pnums;
		
		$list=Db::name('user_voterecord')
            ->field("fromid,actionid,sum(nums) as num,sum(total) as totalall")
            ->where(["action"=>'1',"uid"=>$uid])
            ->group("fromid,actionid,showid")
            ->order("addtime desc")
            ->limit($start,$pnums)
            ->select()
            ->toArray();

        //语言包
        $language_type=$this->language_type;
		foreach($list as $k=>$v){
			$giftinfo=Db::name('gift')
                ->field("giftname,giftname_en")
                ->where("id={$v['actionid']}")
                ->find();
			if(!$giftinfo){
				$giftinfo=array(
					"giftname"=>'GIFT_DELETED',
					"giftname_en"=>lang('GIFT_DELETED')
				);
			}

			if($language=='en'){
				$giftinfo['giftname']=$giftinfo['giftname_en'];
			}

			unset($giftinfo['giftname_en']);

			$list[$k]['giftinfo']=$giftinfo;
            $list[$k]['totalall']=number_format($v['totalall']);
			$userinfo=getUserInfo($v['fromid']);
			if(!$userinfo){
				$userinfo=array(
					"user_nickname"=>lang('USER_DELETED')
				);
			}
			$list[$k]['userinfo']=$userinfo;
		}
		
		$nums=count($list);
		if($nums<$pnums){
			$isscroll=0;
		}else{
			$isscroll=1;
		}
		
		$result=array(
			'data'=>$list,
			'nums'=>$nums,
			'isscroll'=>$isscroll,
		);

		echo json_encode($result);
		
	}
	
	public function liverecord_more()
	{
		$data = $this->request->param();
        $uid= $data['uid'] ?? '';
        $token=$data['token'] ?? '';
        $p=$data['page'] ?? '1';
        $uid=(int)checkNull($uid);
        $token=checkNull($token);
        $p=checkNull($p);
		
		$result=array(
			'data'=>array(),
			'nums'=>0,
			'isscroll'=>0,
		);
	
		if(checkToken($uid,$token)==700){
			echo json_encode($result);
            return;
		} 
		
		$pnums=50;
		$start=($p-1)*$pnums;
		
		$list=Db::name('live_record')
            ->field("starttime,endtime")
            ->where(["uid"=>$uid])
            ->order("starttime desc")
            ->limit($start,$pnums)
            ->select()
            ->toArray();
		foreach($list as $k=>$v){
			$list[$k]['starttime']=date("Y-m-d H:i",$v['starttime']);
			$list[$k]['endtime']=date("Y-m-d H:i",$v['endtime']);
			$cha=$v['endtime']-$v['starttime'];
			$list[$k]['length']=getSeconds($cha,1);
		}

		$nums=count($list);
		if($nums<$pnums){
			$isscroll=0;
		}else{
			$isscroll=1;
		}
		
		$result=array(
			'data'=>$list,
			'nums'=>$nums,
			'isscroll'=>$isscroll,
		);

		echo json_encode($result);
		
	}
	

}
<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-present http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
return [
    'USER_INDEXADMIN_BAN' => 'Ban Member',
    'USER_INDEXADMIN_CANCELBAN' => 'Unban Member',
    'USER_INDEXADMIN_DEFAULT1' => 'User Group',
    'USER_INDEXADMIN_DEFAULT3' => 'Admin Group',
    'USER_INDEXADMIN_INDEX' => 'Site Users',
    'USER_OAUTHADMIN_DELETE' => 'Unbind Third-party User',
    'USER_OAUTHADMIN_INDEX' => 'Third-party Users',
    'USER_INDEXADMIN_DEFAULT' => 'Users',
    'UPLOAD_FILE' => 'Upload file',
];

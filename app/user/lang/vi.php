<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-present http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
return [
    "USERNAME"                      => 'Tên người dùng',
    "NICENAME"                      => 'Biệt danh',
    "AVATAR"                        => 'Ảnh đại diện',
    "EMAIL"                         => 'Email',
    "REGISTRATION_TIME"             => 'Thời gian đăng ký',
    "LAST_LOGIN_TIME"               => 'Thời gian đăng nhập cuối cùng',
    "LAST_LOGIN_IP"                 => 'Địa chỉ IP đăng nhập cuối cùng',
    "STATUS"                        => 'Trạng thái',
    "ACTIONS"                       => "Hành động",
    "USER_STATUS_BLOCKED"           => 'Đã chặn',
    "USER_STATUS_ACTIVATED"         => 'Hoạt động',
    "USER_STATUS_UNVERIFIED"        => 'Chưa xác minh',
    "BLOCK_USER"                    => 'Chặn',
    "BLOCK_USER_CONFIRM_MESSAGE"    => 'Bạn có chắc chắn muốn chặn người dùng này không?',
    "ACTIVATE_USER"                 => 'Kích hoạt',
    "ACTIVATE_USER_CONFIRM_MESSAGE" => 'Bạn có chắc chắn muốn kích hoạt người dùng này không?',
    "THIRD_PARTY_USER"              => "Người dùng bên thứ ba",
    "NOT_FILLED"                    => "Chưa điền",
    "USER_FROM"                     => 'Nguồn',
    "BINGDING_ACCOUNT"              => 'Liên kết tài khoản',
    "FIRST_LOGIN_TIME"              => 'Thời gian đăng nhập đầu tiên',
    "LOGIN_TIMES"                   => 'Số lần đăng nhập',
    'USERNAME_OR_EMAIL_EMPTY'       => "Tên người dùng hoặc Email không được để trống!",
    'PASSWORD_REQUIRED'             => "Mật khẩu không được để trống!",
    'CAPTCHA_REQUIRED'              => "Mã xác nhận không được để trống!",
    'USERNAME_OR_EMAIL'             => 'Tên người dùng hoặc Email',
    'LOGIN_SUCCESS'                 => "Đăng nhập thành công!",
    'PASSWORD_NOT_RIGHT'            => "Mật khẩu không đúng!",
    'CAPTCHA_NOT_RIGHT'             => "Mã xác nhận không đúng!",
    'USERNAME_NOT_EXIST'            => "Tên người dùng không tồn tại!",
    'USER_INDEXADMIN_BAN'           => 'Chặn thành viên',
    'USER_INDEXADMIN_CANCELBAN'     => 'Kích hoạt thành viên',
    'USER_INDEXADMIN_DEFAULT1'      => 'Nhóm người dùng',
    'USER_INDEXADMIN_DEFAULT3'      => 'Nhóm quản trị',
    'USER_INDEXADMIN_INDEX'         => 'Người dùng trang web',
    'USER_OAUTHADMIN_DELETE'        => 'Hủy kết nối người dùng bên thứ ba',
    'USER_OAUTHADMIN_INDEX'         => 'Người dùng bên thứ ba',
    'USER_INDEXADMIN_DEFAULT'       => 'Người dùng',

    'NICKNAME_IS_TO0_LONG'  => 'Biệt danh tối đa 32 ký tự!',
    'SEX_IS_INVALID'        => 'Lựa chọn giới tính không hợp lệ!',
    'BIRTHDAY_IS_INVALID'   => 'Định dạng ngày sinh không đúng!',
    'BIRTHDAY_IS_TOO_EARLY' => 'Bạn sinh quá sớm rồi!',
    'BIRTHDAY_IS_TOO_LATE'  => 'Bạn sinh quá muộn rồi!',
    'URL_FORMAT_IS_WRONG'   => 'Định dạng URL không đúng!',
    'URL_IS_TO0_LONG'       => 'Độ dài URL không được vượt quá 64 ký tự!',
    'SIGNATURE_IS_TO0_LONG' => 'Độ dài chữ ký cá nhân không được vượt quá 128 ký tự!',
    'NO_NEW_INFORMATION'    => "Không có thông tin chỉnh sửa mới!",
    'ERROR'                 => "Lỗi yêu cầu",

    'old_password_is_required'     => 'Mật khẩu cũ không được để trống!',
    'old_password_is_too_long'     => 'Mật khẩu cũ không được vượt quá 32 ký tự!',
    'old_password_is_too_short'    => 'Mật khẩu cũ không được ít hơn 6 ký tự!',
    'password_is_required'         => 'Mật khẩu mới không được để trống!',
    'password_is_too_long'         => 'Mật khẩu mới không được vượt quá 32 ký tự!',
    'password_is_too_short'        => 'Mật khẩu mới không được ít hơn 6 ký tự!',
    'repeat_password_is_required'  => 'Nhập lại mật khẩu không được để trống!',
    'repeat_password_is_too_long'  => 'Nhập lại mật khẩu không được vượt quá 32 ký tự!',
    'repeat_password_is_too_short' => 'Nhập lại mật khẩu không được ít hơn 6 ký tự!',
    'change_success'               => 'Thay đổi thành công!',
    'password_repeat_wrong'        => 'Mật khẩu nhập lại không khớp!',
    'old_password_is_wrong'        => 'Mật khẩu cũ không đúng!',
];

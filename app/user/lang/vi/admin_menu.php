<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-present http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
return [
    'USER_INDEXADMIN_BAN' => 'Chặn thành viên',
    'USER_INDEXADMIN_CANCELBAN' => 'Kích hoạt thành viên',
    'USER_INDEXADMIN_DEFAULT1' => 'Nhóm người dùng',
    'USER_INDEXADMIN_DEFAULT3' => 'Nhóm quản trị',
    'USER_INDEXADMIN_INDEX' => 'Người dùng trang web',
    'USER_OAUTHADMIN_DELETE' => 'Hủy kết nối người dùng bên thứ ba',
    'USER_OAUTHADMIN_INDEX' => 'Người dùng bên thứ ba',
    'USER_INDEXADMIN_DEFAULT' => 'Người dùng',
];

<?php
// +----------------------------------------------------------------------
// | 多语言设置
// +----------------------------------------------------------------------

return [
    // 默认语言
    'default_lang'    => env('LANG_DEFAULT_LANG', 'en'),
    // 允许的语言列表
    'allow_lang_list' => ['vi', 'en', 'ko', 'ja'],
    // 多语言自动侦测变量名
    'detect_var'      => 'language',
    // 是否使用Cookie记录
    'use_cookie'      => true,
    // 多语言cookie变量
    'cookie_var'      => 'think_lang',
    // 扩展语言包
    'extend_list'     => [],
    // Accept-Language转义为对应语言包名称
    'accept_language' => [
        'zh-hans-cn' => 'zh-cn',
        'vi'        => 'vi',
    ],
    // 是否支持语言分组
    'allow_group'     => false,
];
